import os

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from vi_interview.interview_media_view import serve

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('api.urls', namespace='api')),
]

if settings.DEBUG:
    urlpatterns += static('/media/video_interviews/', serve, document_root=os.path.join(settings.MEDIA_ROOT,
                                                                                        'video_interviews'))
    urlpatterns += static('/media/', document_root=settings.MEDIA_ROOT)