# coding=utf-8

from django.db.models import EmailField


class EmailLowerField(EmailField):

    def get_prep_value(self, value):
        # Такая конструкция, потому что непонятно что придет в случае если значение None
        return value.lower() if value else value
