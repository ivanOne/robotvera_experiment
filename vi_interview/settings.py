import os

from kombu import Queue

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'v9@xhhmpr6t7h&7^-*4cr8dsa7g-olof$u17b8l4ga!p3xct^2'

HASHID_FIELD_SALT = '57>md6|[N0xl?S[x8+F`9_=^S~BcM@0_1q!Otg+&`hF7QY=Va(fEL8v)HQ{M7ek7'

MICROSOFT_VIDEO_INDEXER_API_KEY = '9de2aa4e55fc490883fef775063e6c95'

DOMAIN = 'http://0.0.0.0:9000'

DEBUG = True

ALLOWED_HOSTS = ['localhost', '*']

CORS_ORIGIN_ALLOW_ALL = True


# Application definition

INSTALLED_APPS = [
    'raven.contrib.django.raven_compat',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'rest_framework.authtoken',
    'django_filters',
    'rest_framework_swagger',
    'notifications',
    'djcelery_email',

    'users',
    'campaigns',
    'api',
    'dictionary'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

ROOT_URLCONF = 'vi_interview.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'vi_interview.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'vera_dev_vi',
        'HOST': 'db',
        'PORT': '5432',
        'USER': 'vera_vi',
        'PASSWORD': ''
    },
}

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale')
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATICFILES_DIRS = [os.path.join(BASE_DIR, "vi_interview/static")]
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = DOMAIN + '/media/'

AUTH_USER_MODEL = 'users.User'

NOTIFY_USE_JSONFIELD = True
NOTIFICATIONS_SOFT_DELETE = True

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework.filters.SearchFilter',
        'django_filters.rest_framework.DjangoFilterBackend',
        'rest_framework.filters.OrderingFilter'
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 20,
    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.UserRateThrottle',
    ),
    'DEFAULT_THROTTLE_RATES': {
        'user': '240/min'
    }
}

CELERY_TIMEZONE = 'UTC'
CELERY_REDIRECT_STDOUTS = False
CELERY_DEFAULT_QUEUE = 'default'

CELERY_QUEUES = (
    Queue('email'),
    Queue('recognize_emotion'),
    Queue('answer_dist'),
    Queue('add_candidates'),
    Queue('blend_chunk'),
    Queue('concat_interview_chunks'),
    Queue('analyze_emotion'),
    Queue('export_candidates')
)

BROKER_URL = 'amqp://{user}:{password}@{hostname}/{vhost}'.format(
    user='guest',
    password='guest',
    hostname='rabbitmq',
    vhost='/',
)

CACHES_KEYS = {
    'trans_dictionary': '{lang}_{category}_translate_dict'
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'WARNING',
        'handlers': ['console', 'sentry'],
    },
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
        },
        'sentry': {
            'level': 'WARNING',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/tmp/robot-vera-debug.log',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console', 'sentry'],
            'level': 'WARNING',
            'propagate': False,
        },
        'django.db.backends': {
            'handlers': ['console', 'sentry'],
            'level': 'WARNING',
            'propagate': False,
        },
        'django.request': {
            'handlers': ['console', 'sentry'],
            'level': 'ERROR',
            'propagate': False,
        },
        'campaigns': {
            'handlers': ['console', 'sentry'],
            'level': 'WARNING',
            'propagate': False,
        },
        'users': {
            'handlers': ['console', 'sentry'],
            'level': 'WARNING',
            'propagate': False,
        }
    }
}

MEDIA_INTERVIEW = 'video_interviews'
MEDIA_AUDIO_INTERVIEW = 'audio_interviews'
MEDIA_XLS_EXPORTS = 'xls_candidates'

ANALYZE_DIST_API = 'http://51.144.105.1/get_dist'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

CELERY_EMAIL_TASK_CONFIG = {
    'queue': 'email',
    'rate_limit': '15/m',
    'ignore_result': True,
}

PERCENT_DIFF_EMOTION = 20
EMOTIONS_PX_PRECISION = 10
LENGTH_VIDEO_PX = 720

SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'api_key': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        }
    },
}

try:
    from local_settings import *
except ImportError:
    pass
