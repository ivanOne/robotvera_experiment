# noinspection PyUnresolvedReferences
from settings import *

import logging

logging.disable(logging.CRITICAL)

DEBUG = False
TEMPLATE_DEBUG = False

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'vera_dev_vi',
        'HOST': '127.0.0.1',
        'PORT': '5432',
        'USER': 'vera_vi',
        'PASSWORD': 'password'
    }
}

BROKER_BACKEND = 'memory'
BROKER_URL = None
CELERY_ALWAYS_EAGER = True
CELERY_EAGER_PROPAGATES_EXCEPTIONS = True