from django.utils.translation import ugettext as _

DIALOG_TREE = [
    {"title": "",
     "value": _("Hello! My name is Vera, and I'm a robot. And what is your name?"),
     "expanded": True,
     "children": [
         {
             "title": "",
             "value": _("Nice to meet you! Could you tell me what jobs you are interested in?"),
             "expanded": True,
             "children": [
                 {
                     "title": "",
                     "value": _("Thanks! Let me ask you several questions about your last experience. "
                                "How long did you work and what did you do?"),
                     "expanded": True,
                     "children": [
                     {
                         "title": "",
                         "value": _(" What is really important for you, when you think about job offers?"),
                         "expanded": True,
                         "children": [
                             {
                                 "title": "",
                                 "value": _("Ok. What are your salary expectations?"),
                                 "expanded": True,
                                 "children":
                                     [
                                         {
                                             "title": "",
                                             "value": _("Thank you for an interestin dialogue! "
                                                        "I'll send our interview to human source department. "
                                                        "In case of a positive decision, they will contact you as soon "
                                                        "as possible. Have a nice day!")
                                         }
                                     ]
                             }
                         ]
                     }
                     ]
                 }
             ]
         }
     ]
     }
]