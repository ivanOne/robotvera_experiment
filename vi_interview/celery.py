# -*- coding: utf-8 -*-

from __future__ import absolute_import

import celery

from django.conf import settings


class Celery(celery.Celery):

    def on_configure(self):
        if hasattr(settings, 'RAVEN_CONFIG') and settings.RAVEN_CONFIG['dsn']:
            import raven
            from raven.contrib.celery import register_signal, register_logger_signal

            client = raven.Client(settings.RAVEN_CONFIG['dsn'])

            # register a custom filter to filter out duplicate logs
            register_logger_signal(client)

            # hook into the Celery error handler
            register_signal(client)


app = Celery('vi_interview')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

"""
from celery.schedules import crontab
Оставил для примера, пока не надо но вероятно понадобится
app.conf.beat_schedule = {
    'weekday-hourly-vacancy-schedule': {
        'task': 'stafory.tasks.start_schedule',
        'schedule': crontab(minute=0, hour="*/1", day_of_week='1-5'),
    },

    'stop-campaign-every-hour': {
        'options': {'expires': 1800},
        'schedule': datetime.timedelta(hours=1),
        'task': 'stafory.tasks.find_and_stop_campaigns',
    },
}
"""
