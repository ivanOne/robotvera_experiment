# -*- coding: utf-8 -*-
from django.test import TestCase as BaseTestCase
from rest_framework.test import APIClient


class TestCase(BaseTestCase):
    client_class = APIClient
