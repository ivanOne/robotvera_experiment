from django.db import IntegrityError
from django.test import TestCase

from users.models import User


class ModelUserTestCase(TestCase):

    def test_create_ok_user(self):
        user = User.objects.create_user(email='ad@ad.com', password='pass')

        self.assertEqual(user.email, 'ad@ad.com')
        self.assertFalse(user.is_superuser)
        self.assertFalse(user.is_staff)
        self.assertTrue(user.is_active)

    def test_create_ok_superuser(self):
        user = User.objects.create_superuser(email='ad@ad.com', password='pass')

        self.assertEqual(user.email, 'ad@ad.com')
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)
        self.assertTrue(user.is_active)

    def test_exception_unique_email(self):
        User.objects.create_user(email='ad@ad.com', password='pass')

        with self.assertRaises(IntegrityError):
            User.objects.create_superuser(email='ad@ad.com', password='pass')

    def test_email_case_sensitive(self):
        email = 'MyEmail@Gmail.com'
        user = User.objects.create_user(email, password='pass')
        user.refresh_from_db()
        self.assertEqual(user.email, email.lower())