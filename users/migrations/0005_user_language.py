# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-07-18 13:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_auto_20180626_0611'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='language',
            field=models.CharField(choices=[('en', 'English'), ('ru', '\u0420\u0443\u0441\u0441\u043a\u0438\u0439')], default='en', max_length=5, verbose_name='Language'),
        ),
    ]
