# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _


def validate_not_empty(value):
    if not value:
        raise ValidationError(_('%(value)s is empty!'), params={'value': value})
