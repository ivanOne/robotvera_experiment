import os

import xlrd
from django.utils.translation import ugettext as _
from rest_framework.exceptions import ValidationError


def validate_candidate_file(value):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.xlsx', '.xls']
    if not ext.lower() in valid_extensions:
        raise ValidationError(_('Unsupported file extension.'))
    try:
        wb = xlrd.open_workbook(file_contents=value.read())
        value.seek(0)
        if wb.sheets()[0].nrows <= 1:
            raise ValidationError(_('File is empty'))
    except xlrd.XLRDError:
        raise ValidationError(_('Error reading file'))


def validate_video_extension(value):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.webm']
    if not ext.lower() in valid_extensions:
        raise ValidationError(_('Unsupported file extension.'))