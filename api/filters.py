import django_filters

from campaigns.models import Candidate, Campaign


class CandidateFilterSet(django_filters.FilterSet):
    category = django_filters.ChoiceFilter(choices=Campaign.CATEGORY_CHOICES, name='campaign__category')
    invitation_date = django_filters.DateFilter('invitation_date', lookup_expr='contains')

    class Meta:
        model = Candidate
        fields = ['campaign', 'email', 'status', 'category', 'invitation_date']
