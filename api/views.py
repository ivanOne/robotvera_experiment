# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import base64
import pickle

from django.template.loader import get_template
from django.utils.translation import ugettext as _
from django.db.models import Count, Case, When
from django.http import Http404
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from notifications.models import Notification
from rest_framework import generics, viewsets, mixins
from rest_framework.decorators import list_route, detail_route
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status

from api.filters import CandidateFilterSet
from api.pagination import CampaignStatPaginator, NotificationPaginate
from api.permissions import IsOwner, IsCandidateOwner
from campaigns import tasks
from campaigns.models import Campaign, Candidate, InterviewRecordItem, CandidateTranscription, CandidateEmotion
from campaigns.tools import send_interview_entry_mails
from serializers import UserRegistrationSerializer, CampaignSerializer, CandidateSerializer, \
    CandidateBulkUploadSerializer, CandidateInterviewRegistrationSerializer, CampaignStatSerializer, XLSSerializer, \
    ChunkInterviewSerializer, UserSerializer, PasswordSerializer, InterviewFinishSerializer, \
    CandidateListSerializer, NotificationSerializer, TranscriptionSerializer, \
    TranscriptionUpdateHumanScoreSerializer, TranscriptCreateSerializer, EmotionCreateSerializer, \
    CampaignShortSerializer, CampaignPublicInfoSerializer
from users.models import User
from vi_interview.default_tree_interview import DIALOG_TREE


class CampaignViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    pagination_class = CampaignStatPaginator
    queryset = Campaign.objects.unarchived()
    filter_backends = (DjangoFilterBackend, )
    filter_fields = ('category', )

    def handle_exception(self, exc):
        if isinstance(exc, Http404):
            return Response({'detail': _('This interview is no longer available. '
                                         'Please contact the person who shared this link with you.')},
                            status=status.HTTP_404_NOT_FOUND)

        return super(CampaignViewSet, self).handle_exception(exc)

    def get_serializer_class(self):
        if self.action == 'list':
            return CampaignStatSerializer
        else:
            return CampaignSerializer

    def get_queryset(self):
        if self.action == 'list':
            return self.queryset.filter(user=self.request.user).annotate(
                invited=Count(Case(When(campaign_candidates__status=Candidate.Status.INVITED, then=0))),
                finished=Count(Case(When(campaign_candidates__status=Candidate.Status.FINISH_INTERVIEW, then=2))))
        else:
            return self.queryset.filter(user=self.request.user)

    @detail_route(methods=['GET'], url_path='get_campaign_info', permission_classes=(AllowAny, ),
                  serializer_class=CampaignPublicInfoSerializer)
    def get_campaign_info(self, request, pk):
        campaign = get_object_or_404(Campaign.objects.unarchived(), pk=pk)
        serializer = self.serializer_class(campaign)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @list_route(methods=['GET'], url_path='get-categories')
    def get_categories(self, request):
        result = {cat[0]: cat[1] for cat in Campaign.CATEGORY_CHOICES}
        return Response(result, status=status.HTTP_200_OK)

    @list_route(methods=['GET'], url_path='get-user-campaigns', serializer_class=CampaignShortSerializer)
    def get_user_campaigns(self, request):
        campaigns = self.queryset.filter(user=request.user)
        serializer = self.serializer_class(campaigns, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def perform_create(self, serializer):
        campaign = serializer.save(**{'user': self.request.user, 'interview_tree': DIALOG_TREE})
        campaign.email_text = get_template('default_description_campaign.html').render()
        campaign.save()

    def perform_destroy(self, instance):
        instance.delete_to_archive()


class CandidateViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsCandidateOwner)
    serializer_class = CandidateSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filter_class = CandidateFilterSet
    queryset = Candidate.objects.all()
    ordering_fields = ('interview_emotion_positive', 'interview_emotion_negative', 'interview_emotion_happy',
                       'interview_emotion_surprised', 'interview_emotion_sad', 'interview_emotion_angry')

    def get_serializer_class(self):
        if self.action == 'list':
            return CandidateListSerializer
        else:
            return CandidateSerializer

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user, campaign__is_archived=False)

    def perform_destroy(self, instance):
        instance.delete_to_archive()

    def perform_create(self, serializer):
        serializer.save(**{'owner': self.request.user})

    @detail_route(methods=['POST'], url_path='finish-interview', permission_classes=(AllowAny,),
                  serializer_class=InterviewFinishSerializer)
    def finish_interview(self, request, pk):
        """
        POST запрос для проставления финального статуса интервью, в теле необходимо передать:
        interview_duration - длительность видео, поле необязательно.
        Если кандидат не найден, он архивный или имеет неверный статус то ответ 404.
        Если статус проставился ответ будет 200
        Стоит помнить что данный роут необходимо дергать только когда загружены все чанки с аудио и видео,
        иначе повторный вызов этого метода выдаст 404 ошибку
        """
        candidate = get_object_or_404(Candidate.objects.unarchived()
                                      .filter(status=Candidate.Status.START_INTERVIEW),
                                      **{'pk': pk})
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        candidate.interview_duration = serializer.validated_data['interview_duration']
        candidate.interview_status = Candidate.InterviewStatus.END
        candidate.status = Candidate.Status.FINISH_INTERVIEW
        candidate.save()
        if candidate.campaign.interview_type == Campaign.InterviewType.VIDEO_INTERVIEW:
            tasks.concat_all_interview_chunks.delay(str(candidate.pk))
            tasks.analyze_emotion.delay(str(candidate.pk))
        elif candidate.campaign.interview_type == Campaign.InterviewType.AUDIO_INTERVIEW:
            tasks.concat_all_audio_interview_chunks.delay(str(candidate.pk))
        return Response("OK", status=status.HTTP_200_OK)

    @list_route(methods=['POST'],
                url_path='upload-interview-chunk',
                permission_classes=(AllowAny,),
                serializer_class=ChunkInterviewSerializer)
    def upload_interview_chunk(self, request):
        """
        Роут служит для загрузки аудио или видео отрезков видео, в теле запроса принимает
        candidate - id кандидата
        audio, video - файлы отрезков, можно грузить как в месте так и по одиночке главное что бы они относились
        к одной метке времени
        time - метка времени в формате числа

        В случае удачной загрузки вернет код 200, и ответ {'upload': 'success'}
        вернет 400 если не заполнены обязательные поля и если audio или video оба пусты
        вернет 404 если кндидат не найден или у кандидата уже есть файл с интервью
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        valid_data = serializer.validated_data
        interview_item, created = InterviewRecordItem.objects.get_or_create(candidate=valid_data['candidate'],
                                                                            duration=valid_data['duration'])
        if valid_data.get('audio') and not bool(interview_item.audio):
            interview_item.audio = valid_data.get('audio')
        if valid_data.get('video') and not bool(interview_item.video):
            interview_item.video = valid_data.get('video')
        interview_item.text = valid_data.get('text')
        interview_item.save()
        return Response({'upload': 'success'}, status=status.HTTP_200_OK)

    @detail_route(methods=['GET'], url_path='transcription', permission_classes=(IsAuthenticated,),
                  serializer_class=TranscriptionSerializer)
    def get_transcriptions(self, request, pk):
        """
        Роут для получения транскрипции кандидата в виде массива объектов.
        Каждый объект содержит:
        'time' - метка времени,
        'question' - вопрос,
        'answer' - ответ,
        'etalon' - эталонный ответ,
        'human_score' - оценка рекрутера,
        'info' - Дополнительная информация (в данный моент используется под ошибку от нейросети)
        Вернет 404 если кандидат не найден или не принадлежит текущему пользователю
        """
        qs = Candidate.objects.unarchived(owner=request.user)
        candidate = get_object_or_404(qs, pk=pk)
        transcriptions = candidate.transcriptions.all()
        serializer = self.serializer_class(transcriptions, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @detail_route(methods=['GET'], url_path='emotions', permission_classes=(IsAuthenticated,))
    def get_emotions(self, request, pk):
        """
        Роут для получения эмоций кандидата в виде массива объектов.
        Каждый объект содержит набор эмоций (angry, sad, surprised, happy)
        Вернет 404 если кандидат не найден или не принадлежит текущему пользователю
        """
        qs = Candidate.objects.unarchived(owner=self.request.user)
        candidate = get_object_or_404(qs, pk=pk)
        if candidate.emotions_list:
            return Response(candidate.emotions_list, status=status.HTTP_200_OK)
        else:
            # Нужен кастомный ответ для фронта
            return Response({'detail': _('This candidate has no emotions data.')}, status=status.HTTP_404_NOT_FOUND)

    @list_route(methods=['GET'], url_path='get-candidates-stat', permission_classes=(IsAuthenticated,))
    def get_candidates_stat(self, request):
        candidates = self.filter_queryset(self.get_queryset())
        serialize_query = pickle.dumps(candidates.query)
        tasks.export_candidates.delay(request.user.pk, serialize_query)
        return Response(status=status.HTTP_200_OK)


class CandidateBulkCreatedView(generics.GenericAPIView):
    serializer_class = CandidateBulkUploadSerializer
    permission_classes = (IsAuthenticated, )

    def prepare_data(self, data_request, campaign):
        if isinstance(data_request, list):
            data_copy = data_request[:]
            for item in data_copy:
                item.update({'campaign': str(campaign.pk)})
            return data_copy
        else:
            return data_request

    def delete_not_unique(self, validated_data):
        unique_emails = list()
        not_unique_emails = list()
        unique_items = list()
        for item in validated_data:
            email = item['email'].lower()
            if email not in unique_emails:
                unique_emails.append(email)
                unique_items.append(item)
            else:
                not_unique_emails.append(item['email'])
        return not_unique_emails, unique_items

    def post(self, request, *args, **kwargs):
        """
        Принимает список объектов, для массового создания кандидатов.
        Возвращает только статус ответа и {'doubles': ['mail@mail.com']}
        """
        campaign = Campaign.objects.filter(pk=kwargs.get('pk')).first()
        if campaign and campaign.user == request.user:
            data = self.prepare_data(request.data, campaign)
            serializer = self.serializer_class(data=data, many=True)
            serializer.is_valid(raise_exception=True)
            not_unique_emails, unique_items = self.delete_not_unique(serializer.validated_data)
            serializer._validated_data = unique_items
            result = serializer.save(owner=request.user)
            send_interview_entry_mails(result, campaign.email_text, campaign.user)
            return Response({'doubles': not_unique_emails}, status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class InterviewEntryFromEmail(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        """
        Получение данных для интервью, get запрос с get параметром id, это id кандидата
        если кандидат отвечает следующим условиям:
        1. Кандидат есть в базе
        2. Кандидат не в архиве
        3. Кандидат в статусе invited
        Вернется {'interview_tree': {key:val}, 'speaker': 'speaker', 'candidate': 'cand_id', 'candidate_name': 'name'}

        Если кандидат не соответсвует этим требованиям то ошибка 404
        """
        cand_id = request.GET.get('id')
        candidate = Candidate.objects.unarchived().filter(id=cand_id).first()
        if candidate and candidate.status == Candidate.Status.INVITED and not candidate.is_archived and \
                not candidate.campaign.is_archived:
            candidate.status = Candidate.Status.START_INTERVIEW
            candidate.save()
            candidate_name = candidate.name or candidate.email
            response_data = {'interview_tree': candidate.campaign.interview_tree, 'candidate': str(candidate.pk),
                             'speaker': candidate.campaign.speaker, 'candidate_name': candidate_name,
                             'interview_type': candidate.campaign.interview_type}
            return Response(response_data, status=status.HTTP_200_OK)
        elif candidate and candidate.status == Candidate.Status.FINISH_INTERVIEW:
            error_message = _('You have already finished the interview.')

        else:
            error_message = _('This interview is no longer available. Please contact the person who shared this '
                              'link with you.')
        return Response({'detail': error_message}, status=status.HTTP_404_NOT_FOUND)


class InterviewEntryView(generics.GenericAPIView):
    serializer_class = CandidateInterviewRegistrationSerializer
    permission_classes = (AllowAny, )

    def post(self, request, *args, **kwargs):
        """
        Приглашение на интервью, создает кандидата в указанной кампании и возвращает скрипт интервью
        {'interview_tree': {key:val}, 'speaker': 'speaker', 'candidate': 'cand_id'}
        """

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        campaign = serializer.validated_data['campaign']
        candidate = serializer.save(owner=campaign.user, status=Candidate.Status.START_INTERVIEW)
        response_data = {'interview_tree': campaign.interview_tree, 'candidate': str(candidate.pk),
                         'speaker': campaign.speaker, 'interview_type': candidate.campaign.interview_type}
        return Response(response_data, status=status.HTTP_201_CREATED)


class UploadCandidatesXls(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = XLSSerializer

    def post(self, request, *args, **kwargs):
        """
        Загрузка кандидатов через xls файл, при успешной загрузке возвращает статус 200
        Обработка файла и создание кандидатов происходит в фоне
        """
        campaign = Campaign.objects.filter(pk=kwargs['pk']).first()
        if campaign and campaign.user == request.user:
            file_serializer = self.serializer_class(data=request.data)
            file_serializer.is_valid(raise_exception=True)
            candidate_file = file_serializer.validated_data['candidate_file']
            file_data = {'file_name': candidate_file.name,
                         'content': base64.b64encode(candidate_file.read()),
                         'content_type': candidate_file.content_type}
            tasks.add_candidates_in_file.delay(str(campaign.id), file_data)
            return Response('OK', status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class UserViewSet(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get_queryset(self):
        return self.queryset.filter(pk=self.request.user.pk)

    @list_route(methods=['get'])
    def me(self, request):
        """Вернет информацию о текущем пользователе"""
        serializer = self.serializer_class(instance=request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @list_route(methods=['patch'], url_path='change-profile')
    def change_profile(self, request):
        """Изменение профиля пользователя"""
        serializer = self.serializer_class(instance=request.user, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @list_route(methods=['post'], serializer_class=PasswordSerializer, url_path='set-password')
    def set_password(self, request):
        """Изменение пароля пользователя"""
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid(raise_exception=True):
            user = request.user
            if not user.check_password(serializer.validated_data.get('old_password')):
                return Response({'old_password': _('Wrong password')}, status=status.HTTP_400_BAD_REQUEST)
            user.set_password(serializer.data.get('new_password'))
            user.save()
        return Response(status=status.HTTP_200_OK)

    @list_route(methods=['post'], serializer_class=UserRegistrationSerializer, permission_classes=(AllowAny, ))
    def registration(self, request):
        """
        API метод для регистрации пользователя, принимает два параметра email и password.
        При успешной регистрации вернет токен.
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        user = User.objects.create_user(data['email'], data['password'])
        token = user.auth_token.key
        return Response({'token': token}, status=status.HTTP_201_CREATED)


class NotificationViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = NotificationSerializer
    pagination_class = NotificationPaginate
    permission_classes = (IsAuthenticated,)
    queryset = Notification.objects.all()

    def get_queryset(self):
        qs = Notification.objects.active().filter(recipient=self.request.user).order_by('-unread', '-timestamp')
        return qs

    @list_route(methods=['get'], url_path='unread-count')
    def unread_count(self, request):
        """
        Вернет счетчик непрочитанных нотификаций в формате {'count': 5}
        """
        return Response({'count': Notification.objects.unread().filter(recipient=request.user).count()},
                        status=status.HTTP_200_OK)

    @list_route(methods=['post'], url_path='read-notifications')
    def read_notifications(self, request):
        """
        Помечает все нотификации юзера как прочитано, всегда возвращает только статус 200
        """
        if request.POST.get('notifications'):
            id_list = []
            for notify_id in request.POST.getlist('notifications'):
                try:
                    id_list.append(int(notify_id))
                except ValueError:
                    pass
            Notification.objects.filter(recipient=request.user, pk__in=id_list).update(unread=False)
        else:
            Notification.objects.mark_all_as_read(recipient=request.user)
        return Response(status=status.HTTP_200_OK)

    @detail_route(methods=['get'], url_path='get-data')
    def get_data(self, request, pk):
        """
        Вернет дополнительные данные нотификации, если они есть. Если нет то вернется пустой массив
        """
        notify = self.queryset.filter(pk=pk).first()
        data = {}
        if notify:
            data = notify.data
        return Response(data, status=status.HTTP_200_OK)


class TranscriptViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet):
    """
    update - Роут служит для проставления оценки рекрутером, принимает
    'human_score' - оценка в виде числа, в случае успешного обновления вернет {'result': '10.5'}
    """
    serializer_class = TranscriptCreateSerializer
    permission_classes = (AllowAny,)
    queryset = CandidateTranscription.objects.all()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response({'result': instance.candidate.human_score_avg})

    def perform_update(self, serializer):
        serializer.save()
        serializer.instance.candidate.update_avg_human_score()

    def get_queryset(self):
        return CandidateTranscription.objects.filter(candidate__owner=self.request.user)

    def get_serializer_class(self):
        if self.action in ['update', 'partial_update']:
            return TranscriptionUpdateHumanScoreSerializer
        else:
            return self.serializer_class

    def create(self, request, *args, **kwargs):
        """
        Роут для создания транскрипции, принимает:
        'time' - метка времени,
        'question' - вопрос,
        'answer' - ответ,
        'etalon' - эталонный ответ, если есть,
        'candidate' - id кандидата
        Вернет 404 если кандидат не найден или имеет неверный статус
        Вернет 400 если обязательные (time, candidate, question, answer) поля незаполнены
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(status=status.HTTP_201_CREATED, headers=headers)

    def get_permissions(self):
        if self.action in ['update', 'partial_update']:
            self.permission_classes = (IsAuthenticated, )
        return super(TranscriptViewSet, self).get_permissions()


class EmotionViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = EmotionCreateSerializer
    permission_classes = (AllowAny,)
    queryset = CandidateEmotion.objects.all()

    def create(self, request, *args, **kwargs):
        """
        Роут создает эмоции для кандидата, принимает:
        'time' - метка времени,
        'data' - объект с эмоциями,
        'candidate' - id кандидата
        все поля обязательны, если какое то поле незаполнено вернет 400
        если кандидат не найден или имеет неверный статус вернет 404
        в случае успеха вернет код 201
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(status=status.HTTP_201_CREATED, headers=headers)
