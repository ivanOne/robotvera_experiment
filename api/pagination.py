from rest_framework.pagination import LimitOffsetPagination


class CampaignStatPaginator(LimitOffsetPagination):
    default_limit = 50


class NotificationPaginate(LimitOffsetPagination):
    default_limit = 10
