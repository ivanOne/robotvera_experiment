from django.conf import settings
from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token

from rest_framework_swagger.views import get_swagger_view

import views

router = routers.DefaultRouter()
router.register(r'campaign', views.CampaignViewSet)
router.register(r'candidate', views.CandidateViewSet)
router.register(r'emotion', views.EmotionViewSet)
router.register(r'transcript', views.TranscriptViewSet)
router.register(r'user', views.UserViewSet)
router.register(r'notification', views.NotificationViewSet)

urlpatterns = [
    url(r'^api-token-auth/$', obtain_auth_token),
    url(r'^candidate_upload/(?P<pk>\w+)/$', views.CandidateBulkCreatedView.as_view()),
    url(r'^candidate_upload_xls/(?P<pk>\w+)/$', views.UploadCandidatesXls.as_view()),
    url(r'^interview_entry/$', views.InterviewEntryView.as_view()),
    url(r'^interview_entry_from_mail/$', views.InterviewEntryFromEmail.as_view()),
    url(r'^dictionaries/', include('dictionary.urls')),
    url(r'^', include(router.urls)),
]

if settings.DEBUG:
    schema_view = get_swagger_view(url='/api/', patterns=urlpatterns, title='Video interview api doc')
    urlpatterns += [
        url(r'^docs/', schema_view),
    ]