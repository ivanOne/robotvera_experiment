# -*- coding: utf-8 -*-
from django.test import TestCase as BaseTestCase
from rest_framework.test import APIClient

from api.serializers import CandidateListSerializer
from campaigns.models import Campaign, Candidate
from users.models import User

from vcr import VCR

vcr = VCR(
    cassette_library_dir='api/tests/fixtures/vcr/',
    record_mode='once',
    filter_headers=['Authorization'],
    decode_compressed_response=True,
)

VI_FIXTURES = [
    'users/fixtures/users.json'
]


class TestCase(BaseTestCase):
    client_class = APIClient
    fixtures = VI_FIXTURES

    def createUser(self, additional_data={}):
        user = User()
        user.email = 'test@test.com'
        user.name = 'John'
        for key, value in additional_data.items():
            setattr(user, key, value)
        user.save()
        user.set_password('pass')
        return user

    def createCampaign(self, user, additional_data={}):
        campaign = Campaign.objects.create(name='Campaign', category=Campaign.Category.HIRING, user=user,
                                           description='Description', email_text='Mail text',
                                           interview_tree={'root': [{'answer_1': 'text'}, {'answer_2': 'text'}]})
        for key, value in additional_data.items():
            setattr(campaign, key, value)

        campaign.save()
        return campaign

    def createCandidate(self, user, campaign, additional_data={}):
        data = {'email': 'cand@cand.com', 'name': 'Mike', 'note': 'candidate notes', 'campaign': campaign,
                'owner': user}
        candidate = Candidate.objects.create(**data)
        for key, value in additional_data.items():
            setattr(candidate, key, value)

        candidate.save()
        return candidate

    def campaign_to_dict(self, campaign):
        return {
            'id': str(campaign.pk),
            'name': campaign.name,
            'is_archived': campaign.is_archived,
            'user': campaign.user.pk,
            'category': campaign.category,
            'email_text': campaign.email_text,
            'description': campaign.description,
            'interview_tree': campaign.interview_tree,
            'speaker': campaign.speaker,
            'created_at': campaign.created_at.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            'modified_at': campaign.modified_at.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        }

    def campaign_stat_to_dict(self, campaign):
        return {
            'id': str(campaign.pk),
            'name': campaign.name,
            'invited_count': campaign.campaign_candidates.filter(status=Candidate.Status.INVITED).count(),
            'finished_count': campaign.campaign_candidates.filter(status=Candidate.Status.FINISH_INTERVIEW).count(),
            'category': campaign.category
        }

    def candidate_to_dict_in_list(self, candidate):
        return {
            'id': str(candidate.pk),
            'name': candidate.name,
            'email': candidate.email,
            'invitation_date': candidate.invitation_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            'status': candidate.status,
            'note': candidate.note,
            'interview': candidate.interview if candidate.interview else None,
            'audio_interview': candidate.audio_interview if candidate.audio_interview else None,
            'campaign': str(candidate.campaign.pk),
            'human_score_avg': str(candidate.human_score_avg) if candidate.human_score_avg is not None else None,
            'emotions': {
                'positive': candidate.interview_emotion_positive,
                'negative': candidate.interview_emotion_negative,
                'surprised': candidate.interview_emotion_surprised,
                'happy': candidate.interview_emotion_happy,
                'sad': candidate.interview_emotion_sad,
                'angry': candidate.interview_emotion_angry,
                'neutral': candidate.interview_emotion_neutral
            }

        }

    def candidate_to_dict(self, candidate):
        return {
            'id': str(candidate.pk),
            'name': candidate.name,
            'email': candidate.email,
            'invitation_date': candidate.invitation_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            'status': candidate.status,
            'note': candidate.note,
            'interview': candidate.interview if candidate.interview else None,
            'audio_interview': candidate.audio_interview if candidate.audio_interview else None,
            'campaign': str(candidate.campaign.pk),
            'interview_duration': str(candidate.interview_duration),
            'human_score_avg': str(candidate.human_score_avg) if candidate.human_score_avg is not None else None,
            'emotions': {
                'positive': candidate.interview_emotion_positive,
                'negative': candidate.interview_emotion_negative,
                'surprised': candidate.interview_emotion_surprised,
                'happy': candidate.interview_emotion_happy,
                'sad': candidate.interview_emotion_sad,
                'angry': candidate.interview_emotion_angry,
                'neutral': candidate.interview_emotion_neutral
            }
        }