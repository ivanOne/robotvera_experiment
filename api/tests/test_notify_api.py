# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from notifications.models import Notification
from notifications.signals import notify
from rest_framework import status

from api.tests import TestCase
from users.models import User


class GetNotificationTest(TestCase):

    def setUp(self):
        self.user = User.objects.get(pk=1)
        self.user2 = User.objects.get(pk=2)

    def test_get_ok(self):
        notify_1 = {'verb': 'Notify1', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.error}

        notify_2 = {'verb': 'Notify2', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}

        notify_3 = {'verb': 'Notify3', 'recipient': self.user2, 'description': 'Descr',
                    'level': Notification.LEVELS.success}

        notify.send(notify_1['recipient'], **notify_1)
        notify.send(notify_2['recipient'], **notify_2)
        notify.send(notify_3['recipient'], **notify_3)

        self.client.force_authenticate(user=self.user)
        response = self.client.get('/api/notification/')

        data_notify_2 = response.json()['results'][0]
        data_notify_1 = response.json()['results'][1]

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['count'], 2)
        self.assertEqual(data_notify_2['verb'], notify_2['verb'])
        self.assertEqual(data_notify_1['verb'], notify_1['verb'])

    def test_read_and_unread(self):
        notify_1 = {'verb': 'Notify1', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.error}

        notify_2 = {'verb': 'Notify2', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}

        notify_3 = {'verb': 'Notify3', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}
        notify_4 = {'verb': 'Notify4', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}
        notify.send(notify_1['recipient'], **notify_1)
        notify.send(notify_2['recipient'], **notify_2)
        self.client.force_authenticate(user=self.user)
        self.client.post('/api/notification/read-notifications/')
        notify.send(notify_3['recipient'], **notify_3)
        notify.send(notify_4['recipient'], **notify_4)

        response = self.client.get('/api/notification/')
        data = response.json()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(data['results'][0]['unread'])
        self.assertTrue(data['results'][1]['unread'])
        self.assertFalse(data['results'][2]['unread'])
        self.assertFalse(data['results'][3]['unread'])

        self.assertEqual(data['results'][0]['verb'], notify_4['verb'])
        self.assertEqual(data['results'][1]['verb'], notify_3['verb'])
        self.assertEqual(data['results'][2]['verb'], notify_2['verb'])
        self.assertEqual(data['results'][3]['verb'], notify_1['verb'])

    def test_read_all(self):
        notify_1 = {'verb': 'Notify1', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.error}

        notify_2 = {'verb': 'Notify2', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}

        notify_3 = {'verb': 'Notify3', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}
        notify_4 = {'verb': 'Notify4', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}
        notify.send(notify_1['recipient'], **notify_1)
        notify.send(notify_2['recipient'], **notify_2)
        notify.send(notify_3['recipient'], **notify_3)
        notify.send(notify_4['recipient'], **notify_4)

        self.client.force_authenticate(user=self.user)
        self.client.post('/api/notification/read-notifications/')
        response = self.client.get('/api/notification/')
        data = response.json()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(data['results'][0]['unread'])
        self.assertFalse(data['results'][1]['unread'])
        self.assertFalse(data['results'][2]['unread'])
        self.assertFalse(data['results'][3]['unread'])

        self.assertEqual(data['results'][0]['verb'], notify_4['verb'])
        self.assertEqual(data['results'][1]['verb'], notify_3['verb'])
        self.assertEqual(data['results'][2]['verb'], notify_2['verb'])
        self.assertEqual(data['results'][3]['verb'], notify_1['verb'])

    def test_read_ids(self):
        notify_1 = {'verb': 'Notify1', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.error}

        notify_2 = {'verb': 'Notify2', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}

        notify_3 = {'verb': 'Notify3', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}
        notify_4 = {'verb': 'Notify4', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}

        notify.send(notify_1['recipient'], **notify_1)
        notify.send(notify_2['recipient'], **notify_2)
        notify.send(notify_3['recipient'], **notify_3)
        notify.send(notify_4['recipient'], **notify_4)

        self.client.force_authenticate(user=self.user)
        response = self.client.get('/api/notification/')

        ids = []
        for notifi in response.json()['results']:
            ids.append(notifi['pk'])

        response_post = self.client.post('/api/notification/read-notifications/', {'notifications': ids})

        # Эта нотификация будет не прочтеной и должна быть в начале списка
        notify_5 = {'verb': 'Notify5', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}
        notify.send(notify_5['recipient'], **notify_5)

        response_updated_notifications = self.client.get('/api/notification/')
        data = response_updated_notifications.json()

        self.assertEqual(response_post.status_code, status.HTTP_200_OK)
        self.assertEqual(response_updated_notifications.status_code, status.HTTP_200_OK)
        self.assertEqual(data['count'], 5)
        self.assertEqual(data['results'][0]['verb'], 'Notify5')
        self.assertTrue(data['results'][0]['unread'])

        self.assertEqual(data['results'][1]['verb'], 'Notify4')
        self.assertFalse(data['results'][1]['unread'])

        self.assertEqual(data['results'][2]['verb'], 'Notify3')
        self.assertFalse(data['results'][2]['unread'])

        self.assertEqual(data['results'][3]['verb'], 'Notify2')
        self.assertFalse(data['results'][3]['unread'])

        self.assertEqual(data['results'][4]['verb'], 'Notify1')
        self.assertFalse(data['results'][4]['unread'])

    def test_read_with_ids_other_user(self):
        notify_1 = {'verb': 'Notify1', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.error}

        notify_2 = {'verb': 'Notify2', 'recipient': self.user2, 'description': 'Descr',
                    'level': Notification.LEVELS.success}
        notify.send(notify_1['recipient'], **notify_1)
        notify.send(notify_2['recipient'], **notify_2)

        ids = Notification.objects.all().values_list('pk', flat=True)
        self.client.force_authenticate(user=self.user)
        response_post = self.client.post('/api/notification/read-notifications/', {'notifications': ids})
        notify_user_2 = Notification.objects.get(pk=ids[0])
        notify_user_1 = Notification.objects.get(pk=ids[1])

        self.assertEqual(response_post.status_code, status.HTTP_200_OK)

        self.assertEqual(notify_user_2.recipient, self.user2)
        self.assertTrue(notify_user_2.unread)

        self.assertEqual(notify_user_1.recipient, self.user)
        self.assertFalse(notify_user_1.unread)

    def test_get_data(self):
        notify_1 = {'verb': 'Notify1', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.error, 'info': {'data': 'text'}}

        notify.send(notify_1['recipient'], **notify_1)

        self.client.force_authenticate(user=self.user)
        response = self.client.get('/api/notification/')
        data = response.json()
        pk = data['results'][0]['pk']
        response = self.client.get('/api/notification/{}/get-data/'.format(pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.json(), {'info': {'data': 'text'}})

    def test_null_data(self):
        notify_1 = {'verb': 'Notify1', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.error}

        notify.send(notify_1['recipient'], **notify_1)

        self.client.force_authenticate(user=self.user)
        response = self.client.get('/api/notification/')
        data = response.json()
        pk = data['results'][0]['pk']
        response = self.client.get('/api/notification/{}/get-data/'.format(pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNone(response.data)

    def test_unread_count(self):
        notify_1 = {'verb': 'Notify1', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.error}

        notify_2 = {'verb': 'Notify2', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.success}
        notify.send(notify_1['recipient'], **notify_1)
        notify.send(notify_2['recipient'], **notify_2)
        self.client.force_authenticate(user=self.user)
        response = self.client.get('/api/notification/unread-count/')
        self.client.force_authenticate(user=self.user2)
        response_2 = self.client.get('/api/notification/unread-count/')

        self.assertEqual(response.json()['count'], 2)
        self.assertEqual(response_2.json()['count'], 0)

    def test_not_auth_requests(self):
        notify_1 = {'verb': 'Notify1', 'recipient': self.user, 'description': 'Descr',
                    'level': Notification.LEVELS.error}
        notify.send(notify_1['recipient'], **notify_1)
        response_list = self.client.get('/api/notification/')
        response_read_all = self.client.post('/api/notification/read-notifications/')
        response_read_data = self.client.get('/api/notification/1/get-data/')
        response_unread_count = self.client.get('/api/notification/unread-count/')

        self.assertEqual(response_list.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response_read_all.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response_read_data.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response_unread_count.status_code, status.HTTP_401_UNAUTHORIZED)
