# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import timedelta

from django.core import mail
from mock import patch
from django.utils.timezone import now
from rest_framework import status

from api.tests import TestCase
from autofixture import AutoFixture

from campaigns.models import Candidate, Campaign


class CandidateGetTestCase(TestCase):

    def setUp(self):
        self.user = self.createUser()
        self.campaign = self.createCampaign(user=self.user)

        self.other_user = self.createUser(additional_data={'email': 'test2@test.com'})
        self.other_campaign = self.createCampaign(user=self.other_user,
                                                  additional_data={'category': Campaign.Category.ADAPTATION})

        self.candidate = self.createCandidate(self.user, self.campaign)
        self.other_candidates = AutoFixture(Candidate,
                                            field_values={'campaign': self.other_campaign, 'owner': self.other_user},
                                            generate_fk=True).create(5)

    def test_list_ok(self):
        url = '/api/candidate/'
        self.client.force_authenticate(user=self.user)
        response = self.client.get(url, format='json')

        response_candidate = response.data['results'][0]
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(response_candidate['email'], self.candidate.email)

    def test_default_order(self):
        user = self.createUser({'email': 'd@d.com'})
        campaign = self.createCampaign(user=user)
        candidate_1 = self.createCandidate(user, campaign, {'email': 'aw1@c.com'})
        candidate_2 = self.createCandidate(user, campaign, {'email': 'aw2@c.com'})
        candidate_3 = self.createCandidate(user, campaign, {'email': 'aw3@c.com'})

        now_dt = now()
        hour_ago = now_dt - timedelta(hours=1)
        Candidate.objects.filter(pk=candidate_1.pk).update(invitation_date=hour_ago)
        two_hour_ago = now_dt - timedelta(hours=2)
        Candidate.objects.filter(pk=candidate_2.pk).update(invitation_date=two_hour_ago)
        three_hour_ago = now_dt - timedelta(hours=3)
        Candidate.objects.filter(pk=candidate_3.pk).update(invitation_date=three_hour_ago)

        candidate_1.refresh_from_db()
        candidate_2.refresh_from_db()
        candidate_3.refresh_from_db()

        url = '/api/candidate/'
        self.client.force_authenticate(user=user)
        response = self.client.get(url, format='json')

        self.assertEqual(response.data['results'][0], self.candidate_to_dict_in_list(candidate_1))
        self.assertEqual(response.data['results'][1], self.candidate_to_dict_in_list(candidate_2))
        self.assertEqual(response.data['results'][2], self.candidate_to_dict_in_list(candidate_3))

    def test_list_other_user(self):
        url = '/api/candidate/'
        self.client.force_authenticate(user=self.other_user)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 5)

    def test_filter_candidates(self):
        campaign_hiring = self.createCampaign(user=self.other_user, additional_data={
            'category': Campaign.Category.HIRING,
        })
        campaign_assesment = self.createCampaign(user=self.other_user, additional_data={
            'category': Campaign.Category.ASSESSMENT,
        })

        candidates_hiring = AutoFixture(Candidate, field_values={
            'campaign': campaign_hiring, 'owner': self.other_user, 'status': Candidate.Status.INVITED},
                                        generate_fk=True).create(3)

        AutoFixture(Candidate, field_values={
            'campaign': campaign_assesment, 'owner': self.other_user, 'status': Candidate.Status.INVITED},
                    generate_fk=True).create(2)

        url = '/api/candidate/'
        self.client.force_authenticate(user=self.other_user)

        response = self.client.get(url, {'campaign': campaign_hiring.pk}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

        response = self.client.get(url, {'category': Campaign.Category.HIRING}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

        response = self.client.get(url, {'campaign': campaign_assesment.pk}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

        response = self.client.get(url, {'category': Campaign.Category.ASSESSMENT}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

        cand = candidates_hiring[0]
        cand.email = 'f@f.fo'
        cand.save()
        response = self.client.get(url, {'email': cand.email}, format='json')
        self.assertEqual(response.data['results'][0]['email'], cand.email)

        cand = candidates_hiring[0]
        cand.status = Candidate.Status.START_INTERVIEW
        cand.save()
        response = self.client.get(url, {'status': Candidate.Status.START_INTERVIEW}, format='json')
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(response.data['results'][0]['status'], Candidate.Status.START_INTERVIEW)

    def test_search_email_case_insensitive(self):
        email = 'CASE.INSENSITIVEMAIL@MAIL.COM'
        email_1 = 'case.insensItivemail@mail.com'
        campaign_hiring = self.createCampaign(user=self.other_user, additional_data={
            'category': Campaign.Category.HIRING,
        })
        campaign_hiring_1 = self.createCampaign(user=self.other_user, additional_data={
            'category': Campaign.Category.HIRING,
        })
        AutoFixture(Candidate, field_values={
            'email': email,
            'campaign': campaign_hiring,
            'owner': self.other_user,
            'status': Candidate.Status.INVITED
        }, generate_fk=True).create_one()
        AutoFixture(Candidate, field_values={
            'email': email_1,
            'campaign': campaign_hiring_1,
            'owner': self.other_user,
            'status': Candidate.Status.INVITED
        }, generate_fk=True).create_one()

        url = '/api/candidate/'
        self.client.force_authenticate(user=self.other_user)
        response = self.client.get(url, {'email': email}, format='json')

        self.assertEqual(response.data['results'][0]['email'], email.lower())
        self.assertEqual(response.data['results'][1]['email'], email.lower())

    def test_no_auth_user(self):
        url = '/api/candidate/'
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_detail_ok(self):
        url = '/api/candidate/%s/' % self.candidate.pk
        self.client.force_authenticate(user=self.user)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['email'], self.candidate.email)

    def test_fail_get_other_user(self):
        url = '/api/candidate/%s/' % self.other_candidates[0].pk
        self.client.force_authenticate(user=self.user)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_emotion_ordering(self):
        user = self.createUser({'email': 'd@d.com'})
        campaign = self.createCampaign(user=user)
        data_positive_1 = {
            'email': 'aw1@c.com',
            'interview_emotion_positive': 80,
            'interview_emotion_negative': 20,
            'interview_emotion_happy': 36,
            'interview_emotion_surprised': 44,
            'interview_emotion_angry': 15,
            'interview_emotion_sad': 5
        }
        data_positive_2 = {
            'email': 'aw2@c.com',
            'interview_emotion_positive': 70,
            'interview_emotion_negative': 30,
            'interview_emotion_happy': 45,
            'interview_emotion_surprised': 25,
            'interview_emotion_angry': 15,
            'interview_emotion_sad': 15
        }
        data_negative_1 = {
            'email': 'aw3@c.com',
            'interview_emotion_positive': 30,
            'interview_emotion_negative': 70,
            'interview_emotion_happy': 20,
            'interview_emotion_surprised': 10,
            'interview_emotion_angry': 30,
            'interview_emotion_sad': 40
        }
        data_negative_2 = {
            'email': 'aw4@c.com',
            'interview_emotion_positive': 40,
            'interview_emotion_negative': 60,
            'interview_emotion_happy': 35,
            'interview_emotion_surprised': 5,
            'interview_emotion_angry': 25,
            'interview_emotion_sad': 35
        }

        candidate_1 = self.createCandidate(user, campaign, data_positive_1)
        candidate_2 = self.createCandidate(user, campaign, data_positive_2)
        candidate_3 = self.createCandidate(user, campaign, data_negative_1)
        candidate_4 = self.createCandidate(user, campaign, data_negative_2)

        url = '/api/candidate/?ordering=-interview_emotion_positive'
        self.client.force_authenticate(user=user)
        response = self.client.get(url, format='json')
        self.assertEqual(response.data['results'][0]['id'], str(candidate_1.pk))
        self.assertEqual(response.data['results'][1]['id'], str(candidate_2.pk))
        self.assertEqual(response.data['results'][2]['id'], str(candidate_4.pk))
        self.assertEqual(response.data['results'][3]['id'], str(candidate_3.pk))

        url = '/api/candidate/?ordering=-interview_emotion_negative'
        self.client.force_authenticate(user=user)
        response = self.client.get(url, format='json')
        self.assertEqual(response.data['results'][0]['id'], str(candidate_3.pk))
        self.assertEqual(response.data['results'][1]['id'], str(candidate_4.pk))
        self.assertEqual(response.data['results'][2]['id'], str(candidate_2.pk))
        self.assertEqual(response.data['results'][3]['id'], str(candidate_1.pk))

        url = '/api/candidate/?ordering=-interview_emotion_happy'
        self.client.force_authenticate(user=user)
        response = self.client.get(url, format='json')
        self.assertEqual(response.data['results'][0]['id'], str(candidate_2.pk))
        self.assertEqual(response.data['results'][1]['id'], str(candidate_1.pk))
        self.assertEqual(response.data['results'][2]['id'], str(candidate_4.pk))
        self.assertEqual(response.data['results'][3]['id'], str(candidate_3.pk))

        url = '/api/candidate/?ordering=-interview_emotion_surprised'
        self.client.force_authenticate(user=user)
        response = self.client.get(url, format='json')
        self.assertEqual(response.data['results'][0]['id'], str(candidate_1.pk))
        self.assertEqual(response.data['results'][1]['id'], str(candidate_2.pk))
        self.assertEqual(response.data['results'][2]['id'], str(candidate_3.pk))
        self.assertEqual(response.data['results'][3]['id'], str(candidate_4.pk))

        url = '/api/candidate/?ordering=-interview_emotion_angry'
        self.client.force_authenticate(user=user)
        response = self.client.get(url, format='json')
        self.assertEqual(response.data['results'][0]['id'], str(candidate_3.pk))
        self.assertEqual(response.data['results'][1]['id'], str(candidate_4.pk))
        self.assertEqual(response.data['results'][2]['id'], str(candidate_2.pk))
        self.assertEqual(response.data['results'][3]['id'], str(candidate_1.pk))

        url = '/api/candidate/?ordering=-interview_emotion_sad'
        self.client.force_authenticate(user=user)
        response = self.client.get(url, format='json')
        self.assertEqual(response.data['results'][0]['id'], str(candidate_3.pk))
        self.assertEqual(response.data['results'][1]['id'], str(candidate_4.pk))
        self.assertEqual(response.data['results'][2]['id'], str(candidate_2.pk))
        self.assertEqual(response.data['results'][3]['id'], str(candidate_1.pk))


class CandidateCreateTestCase(TestCase):

    def setUp(self):
        self.user = self.createUser()
        self.campaign = self.createCampaign(user=self.user)

    def test_ok(self):
        cand_data = {'email': 'cand@cand.com', 'name': 'Cand name', 'campaign': str(self.campaign.pk), 'note': 'Note'}
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/'
        response = self.client.post(url, cand_data, format='json')
        data = response.json()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(data['email'], 'cand@cand.com')
        self.assertEqual(data['name'], 'Cand name')
        self.assertEqual(data['campaign'], str(self.campaign.pk))
        self.assertEqual(data['note'], 'Note')

    def test_invalidate_create(self):
        invalid_data = {'email': 'candcand.com', 'name': 'Cand name', 'campaign': str(self.campaign.pk)}
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/'
        response = self.client.post(url, invalid_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {u'email': [u'Enter a valid e-mail address.']})

    def test_not_unique_emails(self):
        Candidate.objects.create(email='test@test.com', campaign=self.campaign, owner=self.user)
        cand_data = {'email': 'test@test.com', 'campaign': str(self.campaign.pk)}
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/'
        response = self.client.post(url, cand_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'non_field_errors': ['The fields campaign, e-mail must make a unique set.']})

    def test_other_campaign_invalid(self):
        other_user = self.createUser(additional_data={'email': 'at@at.com'})
        other_campaign = self.createCampaign(other_user)
        cand_data = {'email': 'test@test.com', 'campaign': str(other_campaign.pk)}
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/'
        response = self.client.post(url, cand_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'campaign': ['Access denied.']})

    def test_bulk_ok(self):
        campaign = self.createCampaign(self.user)
        candidates = [{'email': 'cand2@cand.com'},
                      {'email': 'cand@cand.com'},
                      {'email': 'test@test.com'}]
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate_upload/%s/' % campaign.pk
        response = self.client.post(url, candidates, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(campaign.campaign_candidates.count(), 3)
        self.assertEqual(response.data['doubles'], [])
        self.assertEqual(len(mail.outbox), 3)

    def test_bulk_not_unique_in_request(self):
        campaign = self.createCampaign(self.user)
        candidates = [{'email': 'test@test.com'},
                      {'email': 'test@test.com'},
                      {'email': 'test@test.com'},
                      {'email': 'test@test.com'}]
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate_upload/%s/' % campaign.pk
        response = self.client.post(url, candidates, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(campaign.campaign_candidates.count(), 1)
        self.assertEqual(response.data['doubles'][0], 'test@test.com')
        self.assertEqual(len(response.data['doubles']), 3)
        self.assertEqual(len(mail.outbox), 1)

    def test_bulk_not_unique_case_insensitive_in_request(self):
        campaign = self.createCampaign(self.user)
        candidates = [{'email': 'test@test.com'},
                      {'email': 'tesT@test.com'}]
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate_upload/%s/' % campaign.pk
        response = self.client.post(url, candidates, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(campaign.campaign_candidates.count(), 1)
        self.assertEqual(response.data['doubles'][0], 'tesT@test.com')
        self.assertEqual(len(response.data['doubles']), 1)
        self.assertEqual(len(mail.outbox), 1)

    def test_bulk_not_unique_in_db(self):
        campaign = self.createCampaign(self.user)
        self.createCandidate(self.user, campaign, additional_data={'email': 'test@test.com'})
        candidates = [{'email': 'cand2@cand.com'},
                      {'email': 'cand@cand.com'},
                      {'email': 'test@test.com'}]
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate_upload/%s/' % campaign.pk
        response = self.client.post(url, candidates, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), [{}, {}, {'non_field_errors': ['This e-mail already exists']}])
        self.assertEqual(len(mail.outbox), 0)

    def test_bulk_not_unique_case_insensitive_in_db(self):
        campaign = self.createCampaign(self.user)
        self.createCandidate(self.user, campaign, additional_data={'email': 'test@test.com'})
        candidates = [{'email': 'tesT@test.com'}]
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate_upload/%s/' % campaign.pk
        response = self.client.post(url, candidates, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), [{'non_field_errors': ['This e-mail already exists']}])
        self.assertEqual(len(mail.outbox), 0)

    def test_bulk_empty_email(self):
        campaign = self.createCampaign(self.user)
        candidates = [{'email': ''},
                      {'email': 'cand@cand.com'},
                      {'email': 'test@test.com'}]
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate_upload/%s/' % campaign.pk
        response = self.client.post(url, candidates, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), [{'email': ['This field may not be blank.']}, {}, {}])
        self.assertEqual(len(mail.outbox), 0)

    def test_bulk_other_campaign(self):
        campaign = self.createCampaign(self.user)
        candidates = [{'email': 'cand2@cand.com'},
                      {'email': 'cand@cand.com'},
                      {'email': 'test@test.com'}]
        other_user = self.createUser(additional_data={'email': 'v@v.com'})
        self.client.force_authenticate(user=other_user)
        url = '/api/candidate_upload/%s/' % campaign.pk
        response = self.client.post(url, candidates, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(len(mail.outbox), 0)

    def test_bulk_not_auth(self):
        candidates = [{'email': 'cand2@cand.com'},
                      {'email': 'cand@cand.com'},
                      {'email': 'test@test.com'}]
        url = '/api/candidate_upload/%s/' % self.campaign.pk
        response = self.client.post(url, candidates, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(len(mail.outbox), 0)

    @patch('campaigns.tasks.add_candidates_in_file.delay')
    def test_xls_import(self, mock_apply_async):
        self.client.force_authenticate(user=self.user)
        campaign = self.createCampaign(self.user)
        with open('api/tests/files/users.xls') as fp:
            response = self.client.post('/api/candidate_upload_xls/%s/' % campaign.pk, {'candidate_file': fp})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(mock_apply_async.called)

    @patch('campaigns.tasks.add_candidates_in_file.delay')
    def test_xls_invalid_extension(self, mock_apply_async):
        self.client.force_authenticate(user=self.user)
        campaign = self.createCampaign(self.user)
        with open('api/tests/files/users.doc') as fp:
            response = self.client.post('/api/candidate_upload_xls/%s/' % campaign.pk, {'candidate_file': fp})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'candidate_file': ['Unsupported file extension.']})
        self.assertFalse(mock_apply_async.called)

    @patch('campaigns.tasks.add_candidates_in_file.delay')
    def test_xls_invalid_empty_file(self, mock_apply_async):
        self.client.force_authenticate(user=self.user)
        campaign = self.createCampaign(self.user)
        response = self.client.post('/api/candidate_upload_xls/%s/' % campaign.pk)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'candidate_file': ['No file was submitted.']})
        self.assertFalse(mock_apply_async.called)

    @patch('campaigns.tasks.add_candidates_in_file.delay')
    def test_xls_invalid_no_candidates_file(self, mock_apply_async):
        self.client.force_authenticate(user=self.user)
        campaign = self.createCampaign(self.user)
        with open('campaigns/tests/files/empty_users.xls') as fp:
            response = self.client.post('/api/candidate_upload_xls/%s/' % campaign.pk, {'candidate_file': fp})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'candidate_file': ['File is empty']})
        self.assertFalse(mock_apply_async.called)


class CandidateUpdateTestCase(TestCase):

    def setUp(self):
        self.user = self.createUser()
        self.campaign = self.createCampaign(user=self.user)
        self.candidate = self.createCandidate(self.user, self.campaign)

        self.user_1 = self.createUser(additional_data={'email': 'ac@dc.com'})
        self.campaign_1 = self.createCampaign(self.user_1)
        self.candidate_1 = self.createCandidate(self.user_1, self.campaign_1, additional_data={'email': 'f@f.com'})

    def test_put_ok(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/%s/' % self.candidate.pk

        response = self.client.put(url, {'name': 'New name!', 'note': 'New note', 'email': 'new_email@email.com',
                                         'campaign': str(self.campaign.pk)}, format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, self.candidate_to_dict(self.candidate))
        self.assertEqual(response.data['name'], 'New name!')
        self.assertEqual(response.data['note'], 'New note')
        self.assertEqual(response.data['email'], 'new_email@email.com')

    def test_patch_ok(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/%s/' % self.candidate.pk

        response = self.client.patch(url, {'name': 'New name!'}, format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, self.candidate_to_dict(self.candidate))
        self.assertEqual(response.data['name'], 'New name!')

    def test_empty_campaign_patch(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/%s/' % self.candidate.pk

        response = self.client.patch(url, {'name': 'New name!', 'campaign': ''}, format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'campaign': ['This field may not be null.']})

    def test_other_campaign_path(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/%s/' % self.candidate.pk

        response = self.client.patch(url, {'name': 'New name!', 'campaign': str(self.campaign_1.pk)}, format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'campaign': ['Access denied.']})

    def test_read_only_update(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/%s/' % self.candidate.pk

        response = self.client.patch(url,
                                     {
                                         'name': 'New name!',
                                         'status': Candidate.Status.FINISH_INTERVIEW,
                                         'human_score_avg': 50
                                     }, format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.candidate.status, Candidate.Status.INVITED)
        self.assertEqual(self.candidate.human_score_avg, None)

    def test_no_auth_update(self):
        url = '/api/candidate/%s/' % self.candidate.pk

        response = self.client.patch(url, {'name': 'New name!'}, format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_other_candidate_update(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/%s/' % self.candidate_1.pk

        response = self.client.patch(url, {'name': 'New name!'}, format='json')

        self.candidate.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CandidateDeleteTestCase(TestCase):

    def setUp(self):
        self.user = self.createUser()
        self.campaign = self.createCampaign(user=self.user)
        self.candidate = self.createCandidate(self.user, self.campaign)

        self.user_1 = self.createUser(additional_data={'email': 'ac@dc.com'})
        self.campaign_1 = self.createCampaign(self.user_1)
        self.candidate_1 = self.createCandidate(self.user_1, self.campaign_1, additional_data={'email': 'f@f.com'})

    def test_ok_delete(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/%s/' % self.candidate.pk

        response = self.client.delete(url)
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(self.candidate.is_archived)

    def test_delete_no_auth(self):
        url = '/api/candidate/%s/' % self.candidate.pk

        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertFalse(self.candidate.is_archived)

    def test_delete_other_candidate(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/candidate/%s/' % self.candidate_1.pk

        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertFalse(self.candidate_1.is_archived)
