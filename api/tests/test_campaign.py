# -*- coding: utf-8 -*-
import os
import uuid

from django.core.files.storage import DefaultStorage
from mock import patch
import xlrd
from django.conf import settings
from notifications.models import Notification
from rest_framework import status

from api.tests import TestCase
from campaigns.models import Campaign, Candidate
from users.models import User
from vi_interview.celery import app
from vi_interview.default_tree_interview import DIALOG_TREE
from autofixture import AutoFixture


class CampaignCreateTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('user@user.com', 'pass')

    def test_ok_campaign(self):
        campaign_data = {
            'name': 'campaing one',
            'category': Campaign.Category.ASSESSMENT,
        }
        url = '/api/campaign/'
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, campaign_data, format='json')
        campaign = Campaign.objects.all().first()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictEqual(response.data, self.campaign_to_dict(campaign))

    def test_build_tree(self):
        campaign_data = {
            'name': 'campaing one',
            'category': Campaign.Category.ASSESSMENT,
        }
        url = '/api/campaign/'
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, campaign_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertListEqual(response.data['interview_tree'], DIALOG_TREE)

    def test_invalid_campaign(self):
        campaign_data = {
            'name': 'campaing one',
            'category': 9,
        }
        url = '/api/campaign/'
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, campaign_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(response.data, {'category': ['"9" is not a valid choice.']})

    def test_no_auth_campaign(self):
        campaign_data = {
            'name': 'campaing one',
            'category': Campaign.Category.ASSESSMENT,
        }
        url = '/api/campaign/'
        response = self.client.post(url, campaign_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class CampaignGetTestCase(TestCase):

    def setUp(self):
        self.user = self.createUser()
        self.user1 = User.objects.create_user('user1@user.com', 'pass')
        self.campaign1 = self.createCampaign(user=self.user)
        self.campaign_user1 = self.createCampaign(user=self.user1)

    def test_get_user_campaign(self):
        url = '/api/campaign/get-user-campaigns/'
        self.client.force_authenticate(user=self.user)
        response = self.client.get(url, format='json')

        self.assertDictEqual(response.data[0], {'id': str(self.campaign1.pk), 'name': self.campaign1.name})

    def test_ok_campaign(self):
        url = '/api/campaign/'
        self.client.force_authenticate(user=self.user)
        response = self.client.get(url, format='json')

        campaign = response.data['results'][0]

        self.assertEqual(response.data['count'], 1)
        self.assertDictEqual(campaign, self.campaign_stat_to_dict(self.campaign1))

    def test_not_auth_campaign(self):
        url = '/api/campaign/'
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_filter_category(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/campaign/'

        camp_assesment = self.createCampaign(user=self.user, additional_data={'category': Campaign.Category.ASSESSMENT})
        camp_exit = self.createCampaign(user=self.user, additional_data={'category': Campaign.Category.EXIT_INTERVIEW})
        camp_survey = self.createCampaign(user=self.user,
                                          additional_data={'category': Campaign.Category.EMPLOYEE_SURVEY})
        camp_adapt = self.createCampaign(user=self.user, additional_data={'category': Campaign.Category.ADAPTATION})

        response = self.client.get(url, {'category': Campaign.Category.HIRING}, format='json')
        self.assertEqual(response.data['count'], 1)
        self.assertDictEqual(response.data['results'][0], self.campaign_stat_to_dict(self.campaign1))

        response = self.client.get(url, {'category': Campaign.Category.ASSESSMENT}, format='json')
        self.assertEqual(response.data['count'], 1)
        self.assertDictEqual(response.data['results'][0], self.campaign_stat_to_dict(camp_assesment))

        response = self.client.get(url, {'category': Campaign.Category.EXIT_INTERVIEW}, format='json')
        self.assertEqual(response.data['count'], 1)
        self.assertDictEqual(response.data['results'][0], self.campaign_stat_to_dict(camp_exit))

        response = self.client.get(url, {'category': Campaign.Category.EMPLOYEE_SURVEY}, format='json')
        self.assertEqual(response.data['count'], 1)
        self.assertDictEqual(response.data['results'][0], self.campaign_stat_to_dict(camp_survey))

        response = self.client.get(url, {'category': Campaign.Category.ADAPTATION}, format='json')
        self.assertEqual(response.data['count'], 1)
        self.assertDictEqual(response.data['results'][0], self.campaign_stat_to_dict(camp_adapt))

    def test_get_detail_campaign(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/campaign/%s/' % self.campaign1.pk
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, self.campaign_to_dict(self.campaign1))

    def test_no_auth_fail_get_detail_campaign(self):
        self.client.force_authenticate(user=self.user1)
        url = '/api/campaign/%s/' % self.campaign1.pk
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_categories(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/campaign/get-categories/'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, {1: 'Assessment', 0: 'Hiring', 3: 'Employee survey',
                                             2: 'Adaptation', 4: 'Exit interview'})

    def test_fail_no_auth_categories(self):
        url = '/api/campaign/get-categories/'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class UpdateCampaignTestCase(TestCase):

    def setUp(self):
        self.user = self.createUser()
        self.campaign = self.createCampaign(user=self.user)

    def test_put_ok(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/campaign/%s/' % self.campaign.pk

        response = self.client.put(url, {'name': 'New name', 'category': Campaign.Category.ASSESSMENT}, format='json')
        self.campaign.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, self.campaign_to_dict(self.campaign))
        self.assertEqual(response.data['name'], 'New name')
        self.assertEqual(response.data['category'], Campaign.Category.ASSESSMENT)

    def test_patch_ok(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/campaign/%s/' % self.campaign.pk

        response = self.client.put(url, {'name': 'New name'}, format='json')
        self.campaign.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, self.campaign_to_dict(self.campaign))
        self.assertEqual(response.data['name'], 'New name')

    def test_invalid_category(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/campaign/%s/' % self.campaign.pk

        response = self.client.put(url, {'name': 'New name', 'category': 10}, format='json')
        self.campaign.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(response.data, {'category': ['"10" is not a valid choice.']})
        # default cat HIRING
        self.assertEqual(self.campaign.category, Campaign.Category.HIRING)
        self.assertEqual(self.campaign.name, 'Campaign')

    def test_fail_no_auth(self):
        url = '/api/campaign/%s/' % self.campaign.pk

        response = self.client.put(url, {'name': 'New name', 'category': Campaign.Category.ASSESSMENT}, format='json')
        self.campaign.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(self.campaign.name, 'Campaign')
        self.assertEqual(self.campaign.category, Campaign.Category.HIRING)

    def test_failed_campaign_other_user(self):
        other_user = User.objects.create_user('user_o@gmail.com', 'pass')
        other_campaign = self.createCampaign(other_user)
        url = '/api/campaign/%s/' % other_campaign.pk
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, {'name': 'New name', 'category': Campaign.Category.ASSESSMENT}, format='json')
        other_campaign.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(response.data, {'detail': u'This interview is no longer available. '
                                                       u'Please contact the person who shared this link with you.'})
        self.assertEqual(other_campaign.name, 'Campaign')
        self.assertEqual(other_campaign.category, Campaign.Category.HIRING)

    def test_update_speaker(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/campaign/%s/' % self.campaign.pk

        response = self.client.patch(url, {'speaker': 'jane'}, format='json')
        self.campaign.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, self.campaign_to_dict(self.campaign))
        self.assertEqual(response.data['speaker'], 'jane')


class DeleteCampaignTestCase(TestCase):

    def setUp(self):
        self.user = self.createUser()
        self.campaign = self.createCampaign(user=self.user)

    def test_ok(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/campaign/%s/' % self.campaign.pk
        response = self.client.delete(url)
        self.campaign.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(self.campaign.is_archived)

    def test_archived_related_ok(self):
        AutoFixture(Candidate, field_values={'is_archived': False, 'campaign': self.campaign}, generate_fk=True)\
            .create(5)
        self.client.force_authenticate(user=self.user)
        url = '/api/campaign/%s/' % self.campaign.pk
        response = self.client.delete(url)
        self.campaign.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(self.campaign.is_archived)
        for candidate in self.campaign.campaign_candidates.all():
            self.assertTrue(candidate.is_archived)

    def test_no_auth(self):
        url = '/api/campaign/%s/' % self.campaign.pk
        response = self.client.delete(url)
        self.campaign.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertFalse(self.campaign.is_archived)

    def test_other_user(self):
        other_user = User.objects.create_user('user_o@gmail.com', 'pass')
        other_campaign = self.createCampaign(other_user)
        url = '/api/campaign/%s/' % other_campaign.pk
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(url)
        other_campaign.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertFalse(self.campaign.is_archived)


@patch('campaigns.tasks.uuid')
class CandidateDownloadXmlStat(TestCase):

    def create_uuid_and_build_path(self, uuid_mock):
        generated_uuid = uuid.uuid4()
        uuid_mock.uuid4.return_value = generated_uuid
        path_local_parts = [settings.MEDIA_ROOT, settings.MEDIA_XLS_EXPORTS,
                            '{}-candidates.xls'.format(generated_uuid)]
        local_path = os.path.join(*path_local_parts)
        path_parts = [settings.MEDIA_XLS_EXPORTS, '{}-candidates.xls'.format(generated_uuid)]
        path = os.path.join(*path_parts)
        return local_path, path

    def get_sheet(self, path):
        with open(path, 'rb') as file_obj:
            workbook = xlrd.open_workbook(file_contents=file_obj.read())
        sheet_names = workbook.sheet_names()
        return workbook.sheet_by_name(sheet_names[0])

    def setUp(self):
        app.conf.update(task_always_eager=True)
        self.user = self.createUser()
        self.campaign = self.createCampaign(user=self.user)

        self.other_user = self.createUser(additional_data={'email': 'test3@test.com'})
        self.other_campaign = self.createCampaign(user=self.other_user,
                                                  additional_data={'category': Campaign.Category.ADAPTATION})
        self.url = '/api/candidate/get-candidates-stat/'

    def test_ok(self, uuid_mock):
        AutoFixture(Candidate, field_values={'is_archived': False, 'campaign': self.campaign, 'owner': self.user},
                    generate_fk=True).create(5)
        local_path, path = self.create_uuid_and_build_path(uuid_mock)
        self.client.force_authenticate(user=self.user)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        default_storage = DefaultStorage()
        file_url = default_storage.url(path)

        response = self.client.get('/api/notification/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)

        data_0 = response.data['results'][0]
        self.assertEqual(data_0['verb'], 'Data export to Excel is complete.')
        self.assertEqual(data_0['level'], 'success')
        self.assertEqual(
            data_0['description'],
            "You can download the file with the export results by the <a href='%(link)s' trarget='_blank'>link</a>"
            % {'link': file_url}
        )

        response = self.client.get('/api/notification/{}/get-data/'.format(data_0['pk']))
        self.assertIn('type', response.data)
        self.assertIn('url', response.data)
        self.assertEqual(response.data['type'], 'link')
        self.assertEqual(response.data['url'], file_url)

        sheet = self.get_sheet(local_path)

        self.assertEqual(sheet.nrows, 6)
        self.assertEqual(sheet.ncols, 6)

    def test_base_qs(self, uuid_mock):
        AutoFixture(Candidate, field_values={'is_archived': False, 'campaign': self.campaign, 'owner': self.user},
                    generate_fk=True).create(5)
        AutoFixture(Candidate, field_values={'is_archived': False, 'campaign': self.other_campaign,
                                             'owner': self.other_user},
                    generate_fk=True).create(5)
        local_path, path = self.create_uuid_and_build_path(uuid_mock)
        self.client.force_authenticate(user=self.user)
        self.client.get(self.url)

        sheet = self.get_sheet(local_path)
        self.assertEqual(sheet.nrows, 6)
        self.assertEqual(sheet.ncols, 6)

    def test_empty_qs(self, uuid_mock):
        local_path, path = self.create_uuid_and_build_path(uuid_mock)
        self.client.force_authenticate(user=self.user)
        self.client.get(self.url)
        sheet = self.get_sheet(local_path)
        self.assertEqual(sheet.nrows, 1)

    def test_filter_campaign(self, uuid_mock):
        local_path, path = self.create_uuid_and_build_path(uuid_mock)
        AutoFixture(Candidate, field_values={'is_archived': False, 'campaign': self.campaign, 'owner': self.user},
                    generate_fk=True).create(5)
        campaign2 = self.createCampaign(user=self.user)
        AutoFixture(Candidate, field_values={'is_archived': False, 'campaign': campaign2, 'owner': self.user},
                    generate_fk=True).create(2)

        self.client.force_authenticate(user=self.user)
        self.client.get(self.url+'?campaign={}'.format(str(campaign2.pk)))
        sheet = self.get_sheet(local_path)

        self.assertEqual(sheet.nrows, 3)

        local_path, path = self.create_uuid_and_build_path(uuid_mock)
        self.client.get(self.url + '?campaign={}'.format(str(self.campaign.pk)))
        sheet = self.get_sheet(local_path)

        self.assertEqual(sheet.nrows, 6)

    def test_filter_status(self, uuid_mock):
        local_path, path = self.create_uuid_and_build_path(uuid_mock)
        AutoFixture(Candidate, field_values={
            'is_archived': False,
            'campaign': self.campaign,
            'owner': self.user,
            'status': Candidate.Status.FINISH_INTERVIEW
        }, generate_fk=True).create(3)
        AutoFixture(Candidate, field_values={
            'is_archived': False,
            'campaign': self.campaign,
            'owner': self.user,
            'status': Candidate.Status.START_INTERVIEW
        }, generate_fk=True).create(2)

        self.client.force_authenticate(user=self.user)
        self.client.get(self.url + '?status={}'.format(Candidate.Status.FINISH_INTERVIEW))
        sheet = self.get_sheet(local_path)

        self.assertEqual(sheet.nrows, 4)
        for row in range(1, sheet.nrows):
            self.assertEqual(sheet.row(row)[2].value, 'Finished the interview')

        local_path, path = self.create_uuid_and_build_path(uuid_mock)
        self.client.get(self.url + '?status={}'.format(Candidate.Status.START_INTERVIEW))
        sheet = self.get_sheet(local_path)

        self.assertEqual(sheet.nrows, 3)
        for row in range(1, sheet.nrows):
            self.assertEqual(sheet.row(row)[2].value, 'Started the interview')

    def test_filter_invitation_date(self, uuid_mock):
        local_path, path = self.create_uuid_and_build_path(uuid_mock)
        AutoFixture(Candidate, field_values={
            'is_archived': False,
            'campaign': self.campaign,
            'owner': self.user,
        }, generate_fk=True).create(3)
        candidate_first = Candidate.objects.filter(owner=self.user).first()
        changed_dt = candidate_first.invitation_date.replace(day=candidate_first.invitation_date.day - 1)
        Candidate.objects.filter(pk=candidate_first.pk).update(invitation_date=changed_dt)
        self.client.force_authenticate(user=self.user)
        self.client.get(self.url + '?invitation_date={}'.format(changed_dt.strftime('%Y-%m-%d')))

        sheet = self.get_sheet(local_path)

        self.assertEqual(sheet.nrows, 2)
        for row in range(1, sheet.nrows):
            self.assertEqual(sheet.row(row)[1].value, changed_dt.strftime('%d-%m-%Y %H:%M:%S'))
