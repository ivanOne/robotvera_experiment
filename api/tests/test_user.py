# -*- coding: utf-8 -*-
from rest_framework.authtoken.models import Token
from rest_framework import status

from api.tests import TestCase
from users.models import User


class RegistrationTestCase(TestCase):

    def test_registration_ok(self):
        url = '/api/user/registration/'
        response = self.client.post(url, {'email': 'test@test.com', 'password': 'pass'}, format='json')
        user = User.objects.filter(email='test@test.com').first()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(user.email, 'test@test.com')
        self.assertEqual(response.json()['token'], user.auth_token.key)

    def test_registration_invalidate_email(self):
        url = '/api/user/registration/'
        response = self.client.post(url, {'email': 'test@test', 'password': 'pass'}, format='json')
        user = User.objects.filter(email='test@test.com').first()

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'email': ['Enter a valid e-mail address.']})
        self.assertIsNone(user)

    def test_not_unique_email(self):
        url = '/api/user/registration/'
        User.objects.create_user('test@test.com')
        response = self.client.post(url, {'email': 'test@test.com', 'password': 'pass'}, format='json')
        user_count = User.objects.filter(email='test@test.com').count()

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'email': ['This e-mail address already exists.']})
        self.assertEqual(user_count, 1)

    def test_not_unique_email_uppercase(self):
        url = '/api/user/registration/'
        User.objects.create_user('test@test.com')
        response = self.client.post(url, {'email': 'TesT@test.com', 'password': 'pass'}, format='json')
        user_count = User.objects.filter(email='test@test.com').count()

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'email': ['This e-mail address already exists.']})
        self.assertEqual(user_count, 1)

    def test_empty_fields(self):
        url = '/api/user/registration/'

        # Пустой мейл
        response = self.client.post(url, {'password': 'pass'}, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'email': ['This field is required.']})

        # Пустой пароль
        response = self.client.post(url, {'email': 'test@test.com'}, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'password': ['This field is required.']})


class UserAuthTestCase(TestCase):

    def setUp(self):
        self.user = self.createUser()

    def test_token_auth_ok(self):
        self.user.set_password('password')
        self.user.is_active = True
        self.user.save()
        response = self.client.post('/api/api-token-auth/',
                                    {'username': self.user.email, 'password': 'password'}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['token'], self.user.auth_token.key)

    def test_token_auth_ok_email_uppercase(self):
        self.user.set_password('password')
        self.user.is_active = True
        self.user.save()
        response = self.client.post('/api/api-token-auth/',
                                    {'username': self.user.email.upper(), 'password': 'password'}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['token'], self.user.auth_token.key)

    def test_token_auth_invalid(self):
        self.user.set_password('password')
        response = self.client.post('/api/api-token-auth/', {'username': self.user.email, 'password': 'password2'},
                                    format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('Unable to log in with provided credentials.', response.json()['non_field_errors'])

    def test_token_auth_inactive(self):
        self.user.set_password('password')
        self.user.is_active = False
        self.user.save()
        response = self.client.post('/api/api-token-auth/', {'username': self.user.email, 'password': 'password'},
                                    format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('Unable to log in with provided credentials.', response.json()['non_field_errors'])

    def test_token_auth_empty_username_or_password(self):
        response = self.client.post('/api/api-token-auth/', format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {
            'username': ['This field is required.'],
            'password': ['This field is required.']
        })

    def test_token_access(self):
        detail_url = '/api/user/me/'
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user.auth_token.key)
        response = self.client.get(detail_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, {'email': self.user.email, 'name': self.user.name})

    def test_fail_token_access(self):
        detail_url = '/api/user/me/'
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + 'blablabla')
        response = self.client.get(detail_url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.json(), {'detail': 'Invalid token.'})

    def test_signal_create_token(self):
        token = Token.objects.get(user=self.user)
        self.assertEqual(token.key, self.user.auth_token.key)

    def test_ok_change_password(self):
        password = '123'
        self.user.set_password(password)
        self.client.force_authenticate(user=self.user)
        response = self.client.post('/api/user/set-password/', {'old_password': password, 'new_password': '12345'})
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(self.user.check_password('12345'))

    def test_not_matching_password(self):
        password = '123'
        self.user.set_password(password)
        self.client.force_authenticate(user=self.user)
        response = self.client.post('/api/user/set-password/', {'old_password': password + '1',
                                                                'new_password': '12345'})
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'old_password': 'Wrong password'})


class ChangeUserTestCase(TestCase):

    def setUp(self):
        self.user = self.createUser(additional_data={'name': 'Mike'})

    def test_change_ok(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.patch('/api/user/change-profile/', {'name': 'Simon'})
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.json(), {'name': 'Simon', 'email': 'test@test.com'})

    def test_forbidden_no_auth(self):
        response = self.client.patch('/api/user/change-profile/', {'name': 'Simon'})
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(self.user.name, 'Mike')

    def test_no_update_email(self):
        old_email = self.user.email
        self.client.force_authenticate(user=self.user)
        response = self.client.patch('/api/user/change-profile/', {'name': 'Simon', 'email': 'test1@test.com'})
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.user.email, old_email)

    def test_me_ok(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get('/api/user/me/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, {'email': self.user.email, 'name': self.user.name})

    def test_me_no_auth(self):
        response = self.client.get('/api/user/me/')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

