# -*- coding: utf-8 -*-
from decimal import Decimal
from unittest import skip

from django.core.files import File

from campaigns.tasks import concat_all_interview_chunks, concat_all_audio_interview_chunks

try:
    import httplib
    from urllib import urlencode
except ImportError:
    from http import client as httplib
    from urllib.parse import urlencode

from mock import patch

from rest_framework import status
from django.core.files.uploadedfile import SimpleUploadedFile

from users.models import User
from campaigns.models import Candidate, InterviewRecordItem, CandidateTranscription, CandidateEmotion, Campaign
from api.tests import TestCase


class InterviewUploadChunksTest(TestCase):

    def setUp(self):
        self.user = User.objects.get(pk=1)
        self.campaign = self.createCampaign(user=self.user)
        self.candidate = self.createCandidate(campaign=self.campaign, user=self.user)

    def test_ok(self):
        video = SimpleUploadedFile('file.webm', "file_content", content_type="video/mp4")
        response = self.client.post(
            '/api/candidate/upload-interview-chunk/',
            {'candidate': str(self.candidate.id), 'video': video, 'duration': 20000}
        )
        self.candidate.refresh_from_db()
        item = InterviewRecordItem.objects.filter(candidate=self.candidate).first()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(response.content, {'upload': 'success'})
        self.assertEqual(self.candidate.status, Candidate.Status.INVITED)
        self.assertTrue(bool(item.video))

    def test_invalid_empty_files(self):
        response = self.client.post(
            '/api/candidate/upload-interview-chunk/',
            {'candidate': str(self.candidate.id), 'duration': 20000}
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'non_field_errors': ['Audio or video field is required']})

    def test_interview_exists(self):
        self.candidate.interview = SimpleUploadedFile('file.mp4', "file_content", content_type="video/mp4")
        self.candidate.save()
        video = SimpleUploadedFile('file.webm', "file_content", content_type="video/mp4")
        response = self.client.post(
            '/api/candidate/upload-interview-chunk/',
            {'candidate': str(self.candidate.id), 'video': video, 'duration': 20000}
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_candidate_not_exists(self):
        video = SimpleUploadedFile('file.webm', "file_content", content_type="video/mp4")
        response = self.client.post(
            '/api/candidate/upload-interview-chunk/',
            {'filename': '1zzz', 'video': video, 'duration': 15000}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'candidate': ['This field is required.']})


class FinishInterviewAndMergeChunks(TestCase):

    def setUp(self):
        self.user = User.objects.get(pk=1)
        self.campaign = self.createCampaign(user=self.user)
        self.candidate = self.createCandidate(campaign=self.campaign, user=self.user,
                                              additional_data={'status': Candidate.Status.START_INTERVIEW})

    def create_or_update_item(self, candidate, time, audio=None, video=None, text=None):
        item, created = InterviewRecordItem.objects.get_or_create(candidate=candidate, duration=time,
                                                                  defaults={'text': text})
        if audio or video:
            if audio:
                with open(audio) as fp:
                    item.audio.save('audio_file_{}_{}.webm'.format(item.pk, candidate.pk), File(fp))
            if video:
                with open(video) as fp:
                    item.video.save('video_file_{}_{}.webm'.format(item.pk, candidate.pk), File(fp))

    def test_merge_interview(self):
        self.create_or_update_item(self.candidate, 100, audio='campaigns/tests/files/chunks_interview/audio.webm')
        self.create_or_update_item(self.candidate, 100, video='campaigns/tests/files/chunks_interview/video.webm')
        self.create_or_update_item(self.candidate, 200, audio='campaigns/tests/files/chunks_interview/audio (1).webm')
        self.create_or_update_item(self.candidate, 200, video='campaigns/tests/files/chunks_interview/video (1).webm')
        self.create_or_update_item(self.candidate, 300, audio='campaigns/tests/files/chunks_interview/audio (2).webm')
        self.create_or_update_item(self.candidate, 300, video='campaigns/tests/files/chunks_interview/video (2).webm')
        self.create_or_update_item(self.candidate, 400, audio='campaigns/tests/files/chunks_interview/audio (3).webm')
        self.create_or_update_item(self.candidate, 400, video='campaigns/tests/files/chunks_interview/video (3).webm')

        concat_all_interview_chunks(self.candidate.pk)
        self.candidate.refresh_from_db()

        self.assertTrue(self.candidate.interview)

    @skip('Падает в pipline')
    def test_merge_audio_interview(self):
        self.create_or_update_item(self.candidate, 100, audio='campaigns/tests/files/chunks_interview/audio.webm')
        self.create_or_update_item(self.candidate, 200, audio='campaigns/tests/files/chunks_interview/audio.webm',
                                   text='Hello my friend! Have you ever seen snow')

        concat_all_audio_interview_chunks(self.candidate.pk)
        self.candidate.refresh_from_db()

        self.assertTrue(self.candidate.audio_interview)

    @patch('campaigns.tasks.concat_all_interview_chunks.delay')
    @patch('campaigns.tasks.analyze_emotion.delay')
    def test_ok(self, mock_analyze_emotion, mock_concat_all_interview):
        audio = SimpleUploadedFile('audio.webm', "file_content", content_type="video/mp4")
        video = SimpleUploadedFile('video.webm', "file_content", content_type="video/mp4")

        InterviewRecordItem.objects.create(candidate=self.candidate, audio=audio, video=video, duration=20000)
        response = self.client.post('/api/candidate/{}/finish-interview/'.format(self.candidate.pk),
                                    {'interview_duration': 20.5},
                                    format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(mock_concat_all_interview.called)
        self.assertTrue(mock_analyze_emotion.called)
        self.assertEqual(response.data, "OK")
        self.assertEqual(self.candidate.interview_duration, 20.5)
        self.assertEqual(self.candidate.status, Candidate.Status.FINISH_INTERVIEW)
        self.assertEqual(self.candidate.interview_status, Candidate.InterviewStatus.END)

    @patch('campaigns.tasks.concat_all_interview_chunks.delay')
    @patch('campaigns.tasks.concat_all_audio_interview_chunks.delay')
    @patch('campaigns.tasks.analyze_emotion.delay')
    def test_audio_interview_ok(self, mock_analyze_emotion, mock_concat_all_audio_interview, mock_concat_all_interview):
        audio = SimpleUploadedFile('audio.webm', "file_content", content_type="video/mp4")
        self.campaign.interview_type = Campaign.InterviewType.AUDIO_INTERVIEW
        self.campaign.save()

        InterviewRecordItem.objects.create(candidate=self.candidate, audio=audio, duration=20000)
        response = self.client.post('/api/candidate/{}/finish-interview/'.format(self.candidate.pk),
                                    {'interview_duration': 20.5},
                                    format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(mock_concat_all_interview.called)
        self.assertFalse(mock_analyze_emotion.called)
        self.assertTrue(mock_concat_all_audio_interview.called)
        self.assertEqual(response.data, "OK")
        self.assertEqual(self.candidate.interview_duration, 20.5)
        self.assertEqual(self.candidate.status, Candidate.Status.FINISH_INTERVIEW)
        self.assertEqual(self.candidate.interview_status, Candidate.InterviewStatus.END)

    @patch('campaigns.tasks.concat_all_interview_chunks.delay')
    @patch('campaigns.tasks.analyze_emotion.delay')
    def test_full_and_empty_audio(self, mock_analyze_emotion, mock_concat_all_interview):
        audio = SimpleUploadedFile('audio.webm', "file_content", content_type="video/mp4")
        video = SimpleUploadedFile('video.webm', "file_content", content_type="video/mp4")

        InterviewRecordItem.objects.create(candidate=self.candidate, video=video, duration=20000)
        InterviewRecordItem.objects.create(candidate=self.candidate,  audio=audio, duration=20200)
        response = self.client.post('/api/candidate/{}/finish-interview/'.format(self.candidate.pk),
                                    {'interview_duration': 20.5}, format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(mock_concat_all_interview.called)
        self.assertTrue(mock_analyze_emotion.called)
        self.assertEqual(response.data, "OK")
        self.assertEqual(self.candidate.status, Candidate.Status.FINISH_INTERVIEW)
        self.assertEqual(self.candidate.interview_status, Candidate.InterviewStatus.END)

    @patch('campaigns.tasks.concat_all_interview_chunks.delay')
    @patch('campaigns.tasks.analyze_emotion.delay')
    def test_final_interview_cand_not_exist(self, mock_analyze_emotion, mock_concat_all_interview):
        response = self.client.post('/api/candidate/0/finish-interview/', {'interview_duration': 20.5}, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertFalse(mock_concat_all_interview.called)
        self.assertFalse(mock_analyze_emotion.called)

    @patch('campaigns.tasks.concat_all_interview_chunks.delay')
    @patch('campaigns.tasks.analyze_emotion.delay')
    def test_final_interview_candidate_archived(self, mock_analyze_emotion, mock_concat_all_interview):
        self.candidate.status = Candidate.Status.INVITED
        self.candidate.save()
        self.candidate.delete_to_archive()
        response = self.client.post('/api/candidate/{}/finish-interview/'.format(self.candidate.pk),
                                    {'interview_duration': 20.5},
                                    format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertFalse(mock_concat_all_interview.called)
        self.assertFalse(mock_analyze_emotion.called)
        self.assertEqual(self.candidate.status, Candidate.Status.INVITED)
        self.assertEqual(self.candidate.interview_status, Candidate.InterviewStatus.NOT_INTERVIEW)
        self.assertTrue(self.candidate.is_archived)

    @patch('campaigns.tasks.concat_all_interview_chunks.delay')
    @patch('campaigns.tasks.analyze_emotion.delay')
    def test_final_interview_forbidden_status(self, mock_analyze_emotion, mock_concat_all_interview):
        self.candidate.status = Candidate.Status.INVITED
        self.candidate.save()
        response = self.client.post('/api/candidate/{}/finish-interview/'.format(self.candidate.pk),
                                    {'interview_duration': 20.5},
                                    format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertFalse(mock_concat_all_interview.called)
        self.assertFalse(mock_analyze_emotion.called)
        self.assertEqual(self.candidate.status, Candidate.Status.INVITED)
        self.assertEqual(self.candidate.interview_status, Candidate.InterviewStatus.NOT_INTERVIEW)

    @patch('campaigns.tasks.concat_all_interview_chunks.delay')
    @patch('campaigns.tasks.analyze_emotion.delay')
    def test_final_interview_empty_duration(self, mock_analyze_emotion, mock_concat_all_interview):
        self.candidate.status = Candidate.Status.START_INTERVIEW
        self.candidate.save()
        response = self.client.post('/api/candidate/{}/finish-interview/'.format(self.candidate.pk), format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertFalse(mock_concat_all_interview.called)
        self.assertFalse(mock_analyze_emotion.called)
        self.assertEqual(response.data, {'interview_duration': ['This field is required.']})


class TranscriptionCreateTestCase(TestCase):

    def setUp(self):
        self.url = '/api/transcript/'
        self.user = User.objects.get(pk=1)
        self.campaign = self.createCampaign(user=self.user)
        self.candidate = self.createCandidate(campaign=self.campaign, user=self.user,
                                              additional_data={'status': Candidate.Status.START_INTERVIEW})

    def test_create_ok(self):
        data = {'question': 'question', 'answer': 'answer', 'candidate': str(self.candidate.pk), 'duration': 1000}
        response = self.client.post(self.url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        transcript = self.candidate.transcriptions.all()
        self.assertEqual(transcript.count(), 1)
        self.assertEqual(transcript[0].question, 'question')
        self.assertEqual(transcript[0].answer, 'answer')
        self.assertIsNone(transcript[0].etalon)

    def test_wrong_status(self):
        self.candidate.status = Candidate.Status.INVITED
        self.candidate.save()
        data = {'question': 'question', 'answer': 'answer', 'candidate': str(self.candidate.pk), 'duration': 1000}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        self.candidate.status = Candidate.Status.FINISH_INTERVIEW
        self.candidate.save()
        data = {'question': 'question', 'answer': 'answer', 'candidate': str(self.candidate.pk), 'duration': 1000}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_empty_body(self):
        response = self.client.post(self.url, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(response.data, {
            'question': [u'This field is required.'],
            'candidate': [u'This field is required.'],
            'duration': [u'This field is required.']})

    def test_etalon_and_no_answer(self):
        response = self.client.post(self.url, {'question':'q1', 'answer': '', 'etalon': 'etalon',
                                               'candidate': str(self.candidate.pk), 'duration': 1000}, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(response.data, {'answer': [u'Answer field is required']})

    def test_no_candidate(self):
        data = {'question': 'question', 'answer': 'answer', 'candidate': 'fuck_id', 'duration': 1000}
        response = self.client.post(self.url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'candidate': [u'Invalid int or Hashid string']})

    def test_unique_error(self):
        CandidateTranscription.objects.create(candidate=self.candidate, question='question',
                                              answer='answer', duration=10000)
        data = {'question': 'question', 'answer': 'answer', 'candidate': str(self.candidate.pk), 'duration': 10000}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(response.data, {u'non_field_errors': [u'The fields duration, candidate must make a '
                                                                   u'unique set.']})


class TranscriptionGetTestCase(TestCase):

    def setUp(self):
        self.url = '/api/candidate/{}/transcription/'
        self.user = User.objects.get(pk=1)
        self.user_two = User.objects.get(pk=2)

        self.campaign = self.createCampaign(user=self.user)
        self.campaign_two = self.createCampaign(user=self.user_two)
        self.candidate = self.createCandidate(campaign=self.campaign, user=self.user,
                                              additional_data={'status': Candidate.Status.START_INTERVIEW})
        self.candidate_two = self.createCandidate(campaign=self.campaign_two, user=self.user_two,
                                                  additional_data={'status': Candidate.Status.START_INTERVIEW})

        self.transcript_1 = CandidateTranscription.objects.create(candidate=self.candidate, question='question',
                                                                  answer='answer', duration=10000)
        self.transcript_2 = CandidateTranscription.objects.create(candidate=self.candidate, question='question',
                                                                  answer='answer', duration=20000)
        self.transcript_3 = CandidateTranscription.objects.create(candidate=self.candidate, question='question',
                                                                  answer='answer', duration=30000)
        self.transcript_4 = CandidateTranscription.objects.create(candidate=self.candidate_two, question='question',
                                                                  answer='answer', duration=20000)

    def test_get_ok(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url.format(self.candidate.pk), format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data[0]['duration'], 10000)
        self.assertEqual(response.data[1]['duration'], 20000)
        self.assertEqual(response.data[2]['duration'], 30000)

    def test_get_transcription_other_candidate(self):
        self.client.force_authenticate(user=self.user_two)
        response = self.client.get(self.url.format(self.candidate.pk), format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_transcription_no_auth(self):
        response = self.client.get(self.url.format(self.candidate.pk), format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class TranscriptHumanScoreTestCase(TestCase):

    def setUp(self):
        self.url = '/api/transcript/{}/'
        self.user = User.objects.get(pk=1)
        self.user_two = User.objects.get(pk=2)

        self.campaign = self.createCampaign(user=self.user)
        self.campaign_two = self.createCampaign(user=self.user_two)
        self.candidate = self.createCandidate(campaign=self.campaign, user=self.user,
                                              additional_data={'status': Candidate.Status.START_INTERVIEW})
        self.candidate_two = self.createCandidate(campaign=self.campaign_two, user=self.user_two,
                                                  additional_data={'status': Candidate.Status.START_INTERVIEW})

        self.transcript_1 = CandidateTranscription.objects.create(candidate=self.candidate, question='question',
                                                                  answer='answer', duration=10000)
        self.transcript_2 = CandidateTranscription.objects.create(candidate=self.candidate, question='question',
                                                                  answer='answer', duration=20000)
        self.transcript_3 = CandidateTranscription.objects.create(candidate=self.candidate, question='question',
                                                                  answer='answer', duration=30000)
        self.transcript_4 = CandidateTranscription.objects.create(candidate=self.candidate_two, question='question',
                                                                  answer='answer', duration=20000)

    def test_add_human_score_ok(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(self.url.format(self.transcript_1.pk), {'human_score': 5}, format='json')
        self.transcript_1.refresh_from_db()
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['result'], 5.0000)
        self.assertEqual(self.transcript_1.human_score, 5)
        self.assertEqual(self.candidate.human_score_avg, 5.0000)

    def test_avg_function(self):
        self.transcript_1.human_score = 5
        self.transcript_1.save()
        self.transcript_2.human_score = 5
        self.transcript_2.save()
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(self.url.format(self.transcript_3.pk), {'human_score': 3}, format='json')
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.candidate.human_score_avg, Decimal('4.3333'))

    def test_add_human_score_other_candidate(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(self.url.format(self.transcript_4.pk), {'human_score': 5}, format='json')
        self.transcript_4.refresh_from_db()
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertIsNone(self.transcript_4.human_score)
        self.assertIsNone(self.candidate.human_score_avg)

    def test_add_human_score_no_auth(self):
        response = self.client.patch(self.url.format(self.transcript_4.pk), {'human_score': 5}, format='json')
        self.transcript_4.refresh_from_db()
        self.candidate.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertIsNone(self.transcript_4.human_score)
        self.assertIsNone(self.candidate.human_score_avg)


class EmotionCreateTestCase(TestCase):

    def setUp(self):
        self.url = '/api/emotion/'
        self.user = User.objects.get(pk=1)

        self.data = {
            "angry": 0.2579965461055998,
            "happy": 0.026093603990229232,
            "surprised": 0.008477354256741664,
            "sad": 0.01743300462813503
        }
        self.campaign = self.createCampaign(user=self.user)
        self.candidate = self.createCandidate(campaign=self.campaign, user=self.user,
                                              additional_data={'status': Candidate.Status.START_INTERVIEW})

    def test_create_ok(self):
        data = {'data': self.data, 'candidate': str(self.candidate.pk), 'duration': 1000}
        response = self.client.post(self.url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        emotions = self.candidate.emotions.all()
        self.assertEqual(emotions.count(), 1)
        self.assertDictEqual(emotions[0].data, self.data)
        self.assertEqual(emotions[0].duration, 1000)

    def test_create_wrong_status(self):
        self.candidate.status = Candidate.Status.INVITED
        self.candidate.save()
        data = {'data': self.data, 'candidate': str(self.candidate.pk), 'duration': 1000}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        self.candidate.status = Candidate.Status.FINISH_INTERVIEW
        self.candidate.save()
        data = {'data': self.data, 'candidate': str(self.candidate.pk), 'duration': 1000}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_empty_body(self):
        response = self.client.post(self.url, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(response.data, {
            'candidate': [u'This field is required.'],
            'duration': [u'This field is required.']})

    def test_unique_error(self):
        CandidateEmotion.objects.create(candidate=self.candidate, data=self.data, duration=1000)
        data = {'data': self.data, 'candidate': str(self.candidate.pk), 'duration': 1000}
        response = self.client.post(self.url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(response.data, {'non_field_errors': ['The fields duration, '
                                                                  'candidate must make a unique set.']})


class EmotionGetTestCase(TestCase):

    def setUp(self):
        self.url = '/api/candidate/{}/emotions/'
        self.user = User.objects.get(pk=1)
        self.user_two = User.objects.get(pk=2)

        self.data = [
            {
                'time': 200,
                'data': {
                    "angry": 0.2579965461055998,
                    "happy": 0.026093603990229232,
                    "surprised": 0.008477354256741664,
                    "sad": 0.01743300462813503
                }
            },
            {
                'time': 200,
                'data': {
                    "angry": 0.2579965461055998,
                    "happy": 0.026093603990229232,
                    "surprised": 0.008477354256741664,
                    "sad": 0.01743300462813503
                }
            },
        ]

        self.campaign = self.createCampaign(user=self.user)
        self.campaign_two = self.createCampaign(user=self.user_two)
        self.candidate = self.createCandidate(campaign=self.campaign, user=self.user,
                                              additional_data={'status': Candidate.Status.START_INTERVIEW,
                                                               'emotions_list': self.data})
        self.candidate_two = self.createCandidate(campaign=self.campaign_two, user=self.user_two,
                                                  additional_data={'status': Candidate.Status.START_INTERVIEW})

    def test_get_ok(self):
        self.client.force_authenticate(user=self.user)
        self.candidate.emotions_list = ['test']
        self.candidate.save()
        response = self.client.get(self.url.format(self.candidate.pk), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, ['test'])

    def test_get_empty(self):
        self.client.force_authenticate(user=self.user)
        self.candidate.emotions_list = None
        self.candidate.save()
        response = self.client.get(self.url.format(self.candidate.pk), format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data, {'detail': 'This candidate has no emotions data.'})

    def test_get_emotion_other_candidate(self):
        self.client.force_authenticate(user=self.user_two)
        response = self.client.get(self.url.format(self.candidate.pk), format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_emotion_no_auth(self):
        response = self.client.get(self.url.format(self.candidate.pk), format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class InterviewEntryTestCase(TestCase):

    def setUp(self):
        self.user = self.createUser()
        self.campaign = self.createCampaign(user=self.user)

    def test_entry_interview_ok(self):
        cand_data = {'email': 'cand@cand.com', 'name': 'John', 'campaign': str(self.campaign.pk)}
        url = '/api/interview_entry/'
        response = self.client.post(url, cand_data, format='json')
        candidate = Candidate.objects.get(email='cand@cand.com')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictEqual(response.json(), {
            'interview_tree': self.campaign.interview_tree,
            'candidate': str(candidate.pk),
            'speaker': self.campaign.speaker,
            'interview_type': self.campaign.interview_type
        })
        self.assertEqual(candidate.status, Candidate.Status.START_INTERVIEW)

    def test_entry_interview_name_empty(self):
        cand_data = {'email': 'cand@cand.com', 'campaign': str(self.campaign.pk)}
        url = '/api/interview_entry/'
        response = self.client.post(url, cand_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'name': ['This field is required.']})

    def test_entry_interview_invalid_email(self):
        cand_data = {'email': 'candcand.com', 'name': 'John', 'campaign': str(self.campaign.pk)}
        url = '/api/interview_entry/'
        response = self.client.post(url, cand_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'email': ['Enter a valid e-mail address.']})

    def test_entry_interview_mail_exist(self):
        self.createCandidate(user=self.user, campaign=self.campaign, additional_data={'email': 'cand@cand.com'})
        cand_data = {'email': 'cand@cand.com', 'name': 'John', 'campaign': str(self.campaign.pk)}
        url = '/api/interview_entry/'
        response = self.client.post(url, cand_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'non_field_errors': ['The fields campaign, e-mail must make a unique set.']})

    def test_entry_interview_mail_exist_uppercase(self):
        self.createCandidate(user=self.user, campaign=self.campaign, additional_data={'email': 'cand@cand.com'})
        cand_data = {'email': 'canD@cand.com', 'name': 'John', 'campaign': str(self.campaign.pk)}
        url = '/api/interview_entry/'
        response = self.client.post(url, cand_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'non_field_errors': ['The fields campaign, e-mail must make a unique set.']})

    def test_entry_interview_campaign_not_found(self):
        cand_data = {'email': 'cand@cand.com', 'name': 'John', 'campaign': '9999'}
        url = '/api/interview_entry/'
        response = self.client.post(url, cand_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'campaign': ['This interview is no longer available.']})

    def test_entry_interview_from_email_ok(self):
        candidate = self.createCandidate(user=self.user, campaign=self.campaign,
                                         additional_data={'email': 'cand@cand.com', 'status': Candidate.Status.INVITED})
        url = '/api/interview_entry_from_mail/?id={}'.format(candidate.pk)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['interview_tree'], candidate.campaign.interview_tree)
        self.assertEqual(response.data['candidate'], str(candidate.pk))
        self.assertEqual(response.data['speaker'], str(candidate.campaign.speaker))

    def test_entry_interview_no_candidate(self):
        url = '/api/interview_entry_from_mail/?key=saDAS34'
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(response.data, {'detail': 'This interview is no longer available. '
                                                       'Please contact the person who shared this link with you.'})

    def test_entry_interview_wrong_status(self):
        candidate = self.createCandidate(user=self.user, campaign=self.campaign,
                                         additional_data={'email': 'cand@cand.com',
                                                          'status': Candidate.Status.START_INTERVIEW})
        url = '/api/interview_entry_from_mail/?id={}'.format(candidate.pk)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(response.data, {'detail': 'This interview is no longer available. '
                                                       'Please contact the person who shared this link with you.'})

    def test_entry_interview_archive_candidate(self):
        candidate = self.createCandidate(user=self.user, campaign=self.campaign,
                                         additional_data={'email': 'cand@cand.com',
                                                          'status': Candidate.Status.INVITED, 'is_archived': True})
        url = '/api/interview_entry_from_mail/?id={}'.format(candidate.pk)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(response.data, {'detail': 'This interview is no longer available. '
                                                       'Please contact the person who shared this link with you.'})

    def test_entry_interview_candidate_finish_interview(self):
        candidate = self.createCandidate(user=self.user, campaign=self.campaign,
                                         additional_data={'email': 'cand@cand.com',
                                                          'status': Candidate.Status.FINISH_INTERVIEW})
        url = '/api/interview_entry_from_mail/?id={}'.format(candidate.pk)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(response.data, {'detail': 'You have already finished the interview.'})

    def test_get_cand_name_if_name_empty(self):
        candidate = self.createCandidate(user=self.user, campaign=self.campaign,
                                         additional_data={
                                             'email': 'cand@cand.com',
                                             'status': Candidate.Status.INVITED,
                                             'name': ''
                                         })
        url = '/api/interview_entry_from_mail/?id={}'.format(candidate.pk)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['candidate_name'], candidate.email)

    def test_get_cand_name_if_name_not_empty(self):
        candidate = self.createCandidate(user=self.user, campaign=self.campaign,
                                         additional_data={
                                             'email': 'cand@cand.com',
                                             'status': Candidate.Status.INVITED,
                                             'name': 'user'
                                         })
        url = '/api/interview_entry_from_mail/?id={}'.format(candidate.pk)
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['candidate_name'], candidate.name)