# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from hashid_field.rest import HashidSerializerCharField
from notifications.models import Notification
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.validators import UniqueTogetherValidator
from django.utils.translation import ugettext as _

from api.validators import validate_candidate_file, validate_video_extension
from campaigns.models import Campaign, Candidate, CandidateTranscription, CandidateEmotion, InterviewRecordItem
from users.models import User


class UserRegistrationSerializer(serializers.Serializer):
    email = serializers.EmailField(error_messages={'invalid': _('Enter a valid e-mail address.')})
    password = serializers.CharField()

    def validate(self, attrs):
        if User.objects.filter(email=attrs['email']).exists():
            raise ValidationError({
                'email': [_('This e-mail address already exists.')]
            })
        return attrs


class CampaignSerializer(serializers.ModelSerializer):
    id = HashidSerializerCharField(read_only=True)

    class Meta:
        model = Campaign
        fields = ('id', 'name', 'is_archived', 'user', 'category', 'email_text', 'interview_tree', 'description',
                  'created_at', 'modified_at', 'speaker')
        read_only_fields = ('user', )


class CampaignShortSerializer(serializers.ModelSerializer):
    id = HashidSerializerCharField(read_only=True)

    class Meta:
        model = Campaign
        fields = ('id', 'name')


class CandidateSerializer(serializers.ModelSerializer):
    id = HashidSerializerCharField(read_only=True)
    campaign = serializers.PrimaryKeyRelatedField(
        pk_field=HashidSerializerCharField(source_field='campaigns.Campaign.id'),
        queryset=Campaign.objects.unarchived())
    emotions = serializers.SerializerMethodField()

    def get_emotions(self, obj):
        return {
            'angry': obj.interview_emotion_angry,
            'sad': obj.interview_emotion_sad,
            'positive': obj.interview_emotion_positive,
            'negative': obj.interview_emotion_negative,
            'neutral': obj.interview_emotion_neutral,
            'happy': obj.interview_emotion_happy,
            'surprised': obj.interview_emotion_surprised,
        }

    def validate(self, attrs):
        campaign = attrs.get('campaign')
        if self.context['request'].user != campaign.user:
            raise ValidationError({'campaign': [_('Access denied.')]})
        return attrs

    class Meta:
        model = Candidate
        extra_kwargs = {
            "email": {"error_messages": {"invalid": _("Enter a valid e-mail address.")}},
        }
        validators = [
            UniqueTogetherValidator(
                queryset=Candidate.objects.all(),
                message=_('The fields campaign, e-mail must make a unique set.'),
                fields=('email', 'campaign')
            )
        ]
        fields = ('id', 'name', 'email', 'status', 'note', 'invitation_date', 'interview', 'audio_interview',
                  'campaign', 'status', 'interview_duration', 'human_score_avg', 'emotions')
        read_only_fields = ('campaign', 'status', 'interview', 'audio_interview', 'interview_duration',
                            'human_score_avg')


class CandidateListSerializer(CandidateSerializer):
    id = HashidSerializerCharField(read_only=True)
    emotions = serializers.SerializerMethodField()

    def get_emotions(self, obj):
        return {
            'angry': obj.interview_emotion_angry,
            'sad': obj.interview_emotion_sad,
            'positive': obj.interview_emotion_positive,
            'negative': obj.interview_emotion_negative,
            'neutral': obj.interview_emotion_neutral,
            'happy': obj.interview_emotion_happy,
            'surprised': obj.interview_emotion_surprised,
        }

    class Meta:
        model = Candidate
        fields = ('id', 'name', 'email', 'status', 'note', 'invitation_date', 'campaign',
                  'human_score_avg', 'emotions', 'interview', 'audio_interview')


class CandidateBulkUploadSerializer(serializers.ModelSerializer):
    id = HashidSerializerCharField(read_only=True)
    campaign = serializers.PrimaryKeyRelatedField(
        pk_field=HashidSerializerCharField(source_field='campaigns.Campaign.id'),
        queryset=Campaign.objects.unarchived())

    class Meta:
        model = Candidate
        fields = ('email', 'name', 'campaign', 'id')
        extra_kwargs = {
            'email': {'error_messages': {'required': _('E-mail is empty'),
                                         'invalid': _('Enter a valid e-mail address.')}},
            'name': {'error_messages': {'max_length': _('Make sure that the candidate name has no more than '
                                                        '{max_length} characters')}}
        }
        validators = [
            UniqueTogetherValidator(
                queryset=Candidate.objects.all(),
                message=_('This e-mail already exists'),
                fields=('email', 'campaign')
            )
        ]


class CandidateInterviewRegistrationSerializer(serializers.ModelSerializer):
    campaign = serializers.PrimaryKeyRelatedField(
        pk_field=HashidSerializerCharField(source_field='campaigns.Campaign.id'),
        queryset=Campaign.objects.unarchived(),
        error_messages={'does_not_exist': _('This interview is no longer available.')}
    )
    name = serializers.CharField(required=True, max_length=255)

    class Meta:
        model = Candidate
        extra_kwargs = {
            'email': {'error_messages': {'invalid': _('Enter a valid e-mail address.')}},
        }
        validators = [
            UniqueTogetherValidator(
                queryset=Candidate.objects.all(),
                fields=('email', 'campaign'),
                message=_('The fields campaign, e-mail must make a unique set.'),
            )
        ]
        fields = ('email', 'campaign', 'name')


class CampaignStatSerializer(serializers.ModelSerializer):
    id = HashidSerializerCharField(read_only=True)
    invited_count = serializers.SerializerMethodField()
    finished_count = serializers.SerializerMethodField()

    def get_invited_count(self, obj):
        return obj.invited

    def get_finished_count(self, obj):
        return obj.finished

    class Meta:
        model = Campaign
        fields = ('id', 'name', 'invited_count', 'finished_count', 'category')


class CampaignPublicInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Campaign
        fields = ('name', 'description')


class XLSSerializer(serializers.Serializer):
    candidate_file = serializers.FileField(validators=[validate_candidate_file])


class ChunkInterviewSerializer(serializers.ModelSerializer):
    candidate = serializers.PrimaryKeyRelatedField(
        pk_field=HashidSerializerCharField(source_field='campaigns.Candidate.id'),
        queryset=Candidate.objects.unarchived())
    audio = serializers.FileField(validators=[validate_video_extension], required=False)
    video = serializers.FileField(validators=[validate_video_extension], required=False)

    def validate(self, data):
        candidate_pk = data.get('candidate').pk
        get_object_or_404(Candidate.objects.unarchived().filter(interview='', audio_interview=''), pk=candidate_pk)
        if not data.get('video') and not data.get('audio') and not data.get('text'):
            raise ValidationError(_('Audio or video field is required'))
        return data

    class Meta:
        model = InterviewRecordItem
        fields = ('candidate', 'audio', 'video', 'duration', 'text')


class InterviewFinishSerializer(serializers.ModelSerializer):

    class Meta:
        model = Candidate
        fields = ('interview_duration', )
        extra_kwargs = {'interview_duration': {'required': True}}


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('name', 'email')
        read_only_fields = ('email', )


class PasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


class NotificationSerializer(serializers.ModelSerializer):
    tip = serializers.SerializerMethodField()
    info = serializers.SerializerMethodField()

    def get_tip(self, obj):
        if obj.data and obj.data.get('tip'):
            return obj.data['tip']
        return None

    def get_info(self, obj):
        if obj.data and obj.data.get('info'):
            return obj.data['info']
        return False

    class Meta:
        model = Notification
        fields = ('pk', 'verb', 'level', 'description', 'tip', 'info', 'timestamp', 'unread')


class TranscriptionUpdateHumanScoreSerializer(serializers.ModelSerializer):

    class Meta:
        model = CandidateTranscription
        fields = ('human_score', )


class TranscriptCreateSerializer(serializers.ModelSerializer):
    candidate = serializers.PrimaryKeyRelatedField(
        pk_field=HashidSerializerCharField(source_field='campaigns.Candidate.id'),
        queryset=Candidate.objects.unarchived())

    def validate(self, data):
        candidate_pk = data.get('candidate').pk
        get_object_or_404(Candidate.objects.unarchived().filter(status=Candidate.Status.START_INTERVIEW),
                          pk=candidate_pk)
        if not data.get('answer') and data.get('etalon'):
            raise ValidationError({'answer': _('Answer field is required')})
        return data

    class Meta:
        model = CandidateTranscription
        fields = ['candidate', 'duration', 'question', 'answer', 'etalon']


class TranscriptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = CandidateTranscription
        fields = ['id', 'duration', 'question', 'answer', 'etalon', 'human_score', 'info']


class EmotionCreateSerializer(serializers.ModelSerializer):
    candidate = serializers.PrimaryKeyRelatedField(
        pk_field=HashidSerializerCharField(source_field='campaigns.Candidate.id'),
        queryset=Candidate.objects.unarchived())

    def validate(self, data):
        candidate_pk = data.get('candidate').pk
        get_object_or_404(Candidate.objects.unarchived().filter(status=Candidate.Status.START_INTERVIEW),
                          pk=candidate_pk)
        return data

    class Meta:
        model = CandidateEmotion
        fields = ['candidate', 'duration', 'data']
