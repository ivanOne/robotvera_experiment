#!/usr/bin/env bash
cmp --silent /home/vera/Pipfile.lock ./Pipfile.lock || pipenv install --system --deploy --dev

export DJANGO_SETTINGS_MODULE=vi_interview.settings
python manage.py migrate
python manage.py runserver 0.0.0.0:8000
