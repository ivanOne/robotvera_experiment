#!/usr/bin/env bash

cmp --silent /home/vera/Pipfile.lock ./Pipfile.lock || pipenv install --system --deploy --dev

export DJANGO_SETTINGS_MODULE=vi_interview.settings
celery beat -A vi_interview.celery -s /tmp/celerybeat-schedule --pidfile /tmp/celerybeat.pid -l INFO &
celery worker -A vi_interview.celery -E -c 1 -l INFO
