from django.conf import settings
from django.template.loader import get_template
from django.utils.translation import ugettext as _

from vi_interview.utils import send_mass_html_mail


def send_interview_entry_mails(candidates, campaign_description, sender):
    pic_url = settings.DOMAIN + settings.STATIC_URL + 'mail_header_pic.png'
    subject = _('Invitation to the video interview')
    if sender.sender_email and sender.allow_send_client_email:
        email_sender = sender.sender_email
    else:
        email_sender = settings.EMAIL_SENDER
    messages = []
    for candidate in candidates:
        context = {'pic_url': pic_url, 'link': settings.DOMAIN + '/interview?key=' + str(candidate.pk),
                   'description': campaign_description}
        body = get_template('interview_invitation_mail.html').render(context)
        messages.append((subject, 'text', body, email_sender, [candidate.email]))
    send_mass_html_mail(tuple(messages), fail_silently=False)
