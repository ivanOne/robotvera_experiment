# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from campaigns.models import Campaign, Candidate, CandidateEmotion, CandidateTranscription


class CampaignAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'user',
        'category',
        'is_archived',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'user',
        'category',
        'is_archived',
        'created_at',
        'modified_at',
    )
    search_fields = (
        'name',
    )


class CandidateAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'email',
        'campaign',
        'get_status',
        'is_archived',
    )
    list_filter = (
        'campaign',
        'is_archived',
        'status'
    )
    search_fields = (
        'name',
        'email'
    )

    def get_status(self, obj):
        return obj.get_status_display()
    get_status.short_description = 'Status'


class EmotionAdmin(admin.ModelAdmin):
    list_display = ('duration',)

    list_filter = (
        'candidate',
    )


class TranscriptAdmin(admin.ModelAdmin):
    list_display = ('duration', 'question', 'answer')

    list_filter = (
        'candidate',
    )


admin.site.register(CandidateEmotion, EmotionAdmin)
admin.site.register(CandidateTranscription, TranscriptAdmin)
admin.site.register(Campaign, CampaignAdmin)
admin.site.register(Candidate, CandidateAdmin)
