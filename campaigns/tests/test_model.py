# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from autofixture import AutoFixture
from django.db import IntegrityError
from django.test import TestCase

from campaigns.models import Campaign, Candidate, CandidateEmotion
from campaigns.tasks import analyze_emotion
from users.models import User


class CampaignModelTestCase(TestCase):

    def setUp(self):
        self.active_campaigns = AutoFixture(Campaign, field_values={'is_archived': False}, generate_fk=True).create(5)
        self.archived_campaigns = AutoFixture(Campaign, field_values={'is_archived': True}, generate_fk=True).create(5)

    def test_objects_manager_unarchived(self):
        campaigns = Campaign.objects.unarchived()

        self.assertEqual(campaigns.count(), len(self.active_campaigns))
        self.assertFalse(any(campaigns.values_list('is_archived', flat=True)))

    def test_objects_manager_archived(self):
        campaigns = Campaign.objects.archived()

        self.assertEqual(campaigns.count(), len(self.archived_campaigns))
        self.assertFalse(any(not archived for archived in campaigns.values_list('is_archived', flat=True)))

    def test_objects_manager_all(self):
        campaigns = Campaign.objects.all()

        self.assertEqual(campaigns.count(), len(self.active_campaigns + self.archived_campaigns))

    def test_delete_to_archive(self):
        campaign = AutoFixture(Campaign, field_values={'is_archived': False}, generate_fk=True).create_one()
        campaign_1 = AutoFixture(Campaign, field_values={'is_archived': False}, generate_fk=True).create_one()
        AutoFixture(Candidate, field_values={'is_archived': False, 'campaign': campaign}, generate_fk=True).create(5)
        AutoFixture(Candidate, field_values={'is_archived': False, 'campaign': campaign_1}, generate_fk=True).create(5)
        campaign.delete_to_archive()

        self.assertTrue(campaign.is_archived)
        for candidate in Candidate.objects.filter(campaign=campaign.pk):
            self.assertTrue(candidate.is_archived)

        for candidate in Candidate.objects.filter(campaign=campaign_1.pk):
            self.assertFalse(candidate.is_archived)


class CandidateModelTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('ad@ad.com', 'passs')
        self.campaign = AutoFixture(Campaign, field_values={'is_archived': False, 'owner': self.user},
                                    generate_fk=True).create_one()

        self.active_candidates = AutoFixture(Candidate, field_values={'is_archived': False}, generate_fk=True).create(5)
        self.archived_candidates = AutoFixture(Candidate, field_values={'is_archived': True}, generate_fk=True) \
            .create(5)

    def test_create_ok(self):
        candidate = Candidate.objects.create(email='ad@ad.com', campaign=self.campaign, owner=self.campaign.user)

        self.assertEqual(candidate.status, Candidate.Status.INVITED)
        self.assertEqual(candidate.email, 'ad@ad.com')

    def test_non_unique_email_for_campaign(self):
        Candidate.objects.create(email='ad@ad.com', campaign=self.campaign, owner=self.campaign.user)

        with self.assertRaises(IntegrityError):
            Candidate.objects.create(email='ad@ad.com', campaign=self.campaign, owner=self.campaign.user)

    def test_unique_email_for_campaign(self):
        campaign = AutoFixture(Campaign, field_values={'is_archived': False, 'owner': self.user},
                               generate_fk=True).create_one()
        Candidate.objects.create(email='ad@ad.com', campaign=self.campaign, owner=self.campaign.user)
        Candidate.objects.create(email='ad@ad.com', campaign=campaign, owner=campaign.user)

    def test_objects_manager_unarchived(self):
        candidates = Candidate.objects.unarchived()

        self.assertEqual(candidates.count(), len(self.active_candidates))
        self.assertFalse(any(candidates.values_list('is_archived', flat=True)))

    def test_objects_manager_archived(self):
        candidates = Candidate.objects.archived()

        self.assertEqual(candidates.count(), len(self.archived_candidates))
        self.assertFalse(any(not archived for archived in candidates.values_list('is_archived', flat=True)))

    def test_delete_to_archive_in_model(self):
        candiate = AutoFixture(Candidate, field_values={'is_archived': False}, generate_fk=True).create_one()
        candiate.delete_to_archive()
        candiate.refresh_from_db()

        self.assertTrue(candiate.is_archived)

    def test_email_case_sensitive(self):
        email = 'MyEmail@Gmail.com'
        candidate = Candidate.objects.create(email=email, campaign=self.campaign, owner=self.campaign.user)
        candidate.refresh_from_db()
        self.assertEqual(candidate.email, email.lower())


class CandidateEmotionTestCase(TestCase):

    def setUp(self):
        emotion_data_list = [
            {"data":{
                "angry": 0.08,
                "happy": 0.18,
                "surprised": 0.06,
                "sad": 0.06
            },
                "time": 11100},
            {"data": {
                "angry": 0.09,
                "happy": 0.19,
                "surprised": 0.05,
                "sad": 0.06
            },
                "time": 11201}
        ]
        emotion_data_dict = {"data":
            {
                "angry": 0.52,
                "happy": 0.23,
                "surprised": 0.09,
                "sad": 0.12
            },
            "time": 11500}

        self.happy = int(round(57))
        self.angry = int(round(43))
        self.surprised = int(round(0.0))
        self.sad = int(round(0.0))
        self.neutral = 100 - (self.happy + self.angry + self.surprised + self.sad)
        self.positive = self.happy + self.surprised
        self.negative = self.angry + self.sad

        self.user = User.objects.create_user('ad@ad.com', 'passs')
        self.campaign = AutoFixture(Campaign, field_values={'is_archived': False, 'owner': self.user},
                                    generate_fk=True).create_one()

        self.candidate = AutoFixture(Candidate, field_values={'interview_duration': 20000}, generate_fk=True)\
            .create_one()
        CandidateEmotion.objects.create(data=emotion_data_list, candidate=self.candidate, duration=1109)
        CandidateEmotion.objects.create(data=emotion_data_dict, candidate=self.candidate, duration=2000)

    def test_calc_emotions(self):
        overall_data, emotions_data = CandidateEmotion.objects.get_stat_emotions(self.candidate)

        self.assertEqual(overall_data['positive'], self.positive)
        self.assertEqual(overall_data['negative'], self.negative)
        self.assertEqual(emotions_data['happy'], self.happy)
        self.assertEqual(emotions_data['surprised'], self.surprised)
        self.assertEqual(emotions_data['sad'], self.sad)
        self.assertEqual(emotions_data['angry'], self.angry)
        self.assertEqual(emotions_data['neutral'], self.neutral)

    def test_calc_emotions_if_empty_duration(self):
        self.candidate.interview_duration = 0.0
        self.candidate.save()
        overall_data, emotions_data = CandidateEmotion.objects.get_stat_emotions(self.candidate)

        self.candidate.interview_emotion_angry = emotions_data['angry']
        self.candidate.interview_emotion_sad = emotions_data['sad']
        self.candidate.interview_emotion_happy = emotions_data['happy']
        self.candidate.interview_emotion_surprised = emotions_data['surprised']
        self.candidate.interview_emotion_neutral = emotions_data['neutral']
        self.candidate.interview_emotion_positive = overall_data['positive']
        self.candidate.interview_emotion_negative = overall_data['negative']
        self.candidate.save()
        self.candidate.refresh_from_db()

        self.assertEqual(self.candidate.interview_emotion_angry, 0)
        self.assertEqual(self.candidate.interview_emotion_sad, 0)
        self.assertEqual(self.candidate.interview_emotion_happy, 0)
        self.assertEqual(self.candidate.interview_emotion_surprised, 0)
        self.assertEqual(self.candidate.interview_emotion_neutral, 0)
        self.assertEqual(self.candidate.interview_emotion_positive, 0)
        self.assertEqual(self.candidate.interview_emotion_negative, 0)

    def test_compression_emotions(self):
        analyze_emotion(str(self.candidate.pk))
        self.candidate.refresh_from_db()

        self.assertIsNotNone(self.candidate.emotions_list)
        self.assertQuerysetEqual(self.candidate.emotions.all(), [])
        self.assertIsNotNone(self.candidate.emotion_backup)

    def test_compression_no_emotions(self):
        candidate_no_emotions = AutoFixture(Candidate, field_values={'interview_duration': 20000}, generate_fk=True)\
            .create_one()
        analyze_emotion(str(candidate_no_emotions.pk))
        candidate_no_emotions.refresh_from_db()

        self.assertEqual(candidate_no_emotions.emotions_list, [])
        self.assertIsNotNone(candidate_no_emotions.emotion_backup)