# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import base64
import os

from autofixture import AutoFixture
from django.core import mail
from django.test import TestCase
from notifications.models import Notification

from campaigns.models import Campaign, Candidate
from campaigns.tasks import add_candidates_in_file
from users.models import User


class TaskUploadCandidatesFromXml(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('ad@ad.com', 'passs')
        self.campaign = AutoFixture(Campaign, field_values={'is_archived': False, 'user': self.user},
                                    generate_fk=True).create_one()

    def read_and_encode(self, file_path, content_type='application/vnd.ms-excel'):
        with open(file_path) as fp:
            content = base64.b64encode(fp.read())
        return {'file_name': os.path.basename(fp.name), 'content': content, 'content_type': content_type}

    def test_success_upload(self):
        file_data = self.read_and_encode('campaigns/tests/files/users.xls')
        add_candidates_in_file(str(self.campaign.pk), file_data)
        candidates = self.campaign.campaign_candidates.all()

        self.assertEqual(candidates.count(), 3)

    def test_double_users(self):
        file_data = self.read_and_encode('campaigns/tests/files/double_users.xls')
        add_candidates_in_file(str(self.campaign.pk), file_data)
        candidates = self.campaign.campaign_candidates.all()

        self.assertEqual(candidates.count(), 3)

    def test_doubles_users_in_db(self):
        file_data = self.read_and_encode('campaigns/tests/files/users.xls')
        Candidate.objects.create(campaign=self.campaign, email='some1@email.com', owner=self.user)
        add_candidates_in_file(str(self.campaign.pk), file_data)
        candidates = self.campaign.campaign_candidates.all()

        self.assertEqual(candidates.count(), 1)

    def test_invalid_file_users(self):
        file_data = self.read_and_encode('campaigns/tests/files/this_not_excel.docx')

        add_candidates_in_file(str(self.campaign.pk), file_data)
        candidates = self.campaign.campaign_candidates.all()

        self.assertEqual(candidates.count(), 0)

    def test_no_email_in_file(self):
        file_data = self.read_and_encode('campaigns/tests/files/users_no_email.xls')

        add_candidates_in_file(str(self.campaign.pk), file_data)
        candidates = self.campaign.campaign_candidates.all()

        self.assertEqual(candidates.count(), 0)

    def test_double_upper_case_email_in_file(self):
        file_data = self.read_and_encode('campaigns/tests/files/double_users_upper_case_doubles.xls')
        add_candidates_in_file(str(self.campaign.pk), file_data)
        candidates = self.campaign.campaign_candidates.all()

        self.assertEqual(candidates.count(), 1)

    def test_doubles_upper_case_email_in_db(self):
        file_data = self.read_and_encode('campaigns/tests/files/users_upper_case_doubles.xls')
        Candidate.objects.create(campaign=self.campaign, email='some1@email.com', owner=self.user)
        add_candidates_in_file(str(self.campaign.pk), file_data)
        candidates = self.campaign.campaign_candidates.all()

        self.assertEqual(candidates.count(), 1)


class TestNotificationUploadCandidates(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('ad@ad.com', 'passs')
        self.campaign = AutoFixture(Campaign, field_values={'is_archived': False, 'user': self.user},
                                    generate_fk=True).create_one()

    def read_and_encode(self, file_path, content_type='application/vnd.ms-excel'):
        with open(file_path) as fp:
            content = base64.b64encode(fp.read())
        return {'file_name': os.path.basename(fp.name), 'content': content, 'content_type': content_type}

    def test_success_upload(self):
        file_data = self.read_and_encode('campaigns/tests/files/users.xls')
        add_candidates_in_file(str(self.campaign.pk), file_data)
        notification = self.user.notifications.all().first()

        self.assertEqual(notification.verb, 'Upload success')
        self.assertEqual(notification.level, Notification.LEVELS.success)
        self.assertEqual(notification.description, '3 candidates fully uploaded')
        self.assertTrue(notification.unread)
        self.assertEqual(len(mail.outbox), 3)

    def test_double_users(self):
        file_data = self.read_and_encode('campaigns/tests/files/double_users.xls')
        add_candidates_in_file(str(self.campaign.pk), file_data)
        notification = self.user.notifications.all().first()

        self.assertEqual(notification.verb, 'Upload success')
        self.assertEqual(notification.level, Notification.LEVELS.success)
        self.assertEqual(notification.description, '1 duplicates to upload file')
        self.assertTrue(notification.unread)
        self.assertDictEqual(notification.data, {
            'info': True,
            'tip': '',
            'type': 'table',
            'table_data': {
                'table_type': 'candidate_errors',
                'data': [{'lines': '5', 'email': 'some3@email.com', 'event': '1 matches'}],
                'header': {'lines': 'Lines', 'email': 'E-mail', 'event': 'Event'}}
        })
        self.assertEqual(len(mail.outbox), 3)

    def test_many_double_users(self):
        file_data = self.read_and_encode('campaigns/tests/files/many_double_users.xls')
        add_candidates_in_file(str(self.campaign.pk), file_data)
        notification = self.user.notifications.all().first()

        self.assertEqual(notification.verb, 'Upload success')
        self.assertEqual(notification.level, Notification.LEVELS.success)
        self.assertEqual(notification.description, '3 duplicates to upload file')
        self.assertTrue(notification.unread)
        self.assertDictEqual(notification.data, {
            'info': True,
            'tip': '',
            'type': 'table',
            'table_data': {
                'table_type': 'candidate_errors',
                'data': [
                    {'lines': '8', 'email': 'some4@email.com', 'event': '1 matches'},
                    {'lines': '5; 6', 'email': 'some3@email.com', 'event': '2 matches'}
                ],
                'header': {'lines': 'Lines', 'email': 'E-mail', 'event': 'Event'}
            }
        }
        )
        self.assertEqual(len(mail.outbox), 4)

    def test_doubles_users_in_db(self):
        file_data = self.read_and_encode('campaigns/tests/files/users.xls')
        Candidate.objects.create(campaign=self.campaign, email='some1@email.com', owner=self.user)
        add_candidates_in_file(str(self.campaign.pk), file_data)
        notification = self.user.notifications.all().first()

        self.assertEqual(notification.verb, 'Upload was broken')
        self.assertEqual(notification.level, Notification.LEVELS.error)
        self.assertEqual(notification.description, '1 errors in file')
        self.assertTrue(notification.unread)
        self.assertDictEqual(notification.data, {
            'info': True,
            'tip': 'Fix errors in the file and try again',
            'type': 'table',
            'table_data': {
                'table_type': 'candidate_errors',
                'data': [
                    {'lines': '1', 'email': 'some1@email.com', 'event': 'This e-mail already exists'}
                ],
                'header': {'lines': 'Lines', 'email': 'E-mail', 'event': 'Event'}}})
        self.assertEqual(len(mail.outbox), 0)

    def test_no_email_in_file(self):
        file_data = self.read_and_encode('campaigns/tests/files/users_no_email.xls')

        add_candidates_in_file(str(self.campaign.pk), file_data)
        notification = self.user.notifications.all().first()

        self.assertEqual(notification.verb, 'Upload was broken')
        self.assertEqual(notification.level, Notification.LEVELS.error)
        self.assertEqual(notification.description, '1 errors in file')
        self.assertTrue(notification.unread)
        self.assertDictEqual(notification.data, {
            'info': True,
            'tip': 'Fix errors in the file and try again',
            'type': 'table',
            'table_data': {
                'table_type': 'candidate_errors',
                'data': [
                    {'lines': '1', 'email': '', 'event': u'This field may not be blank.'}
                ],
                'header': {'lines': 'Lines', 'email': 'E-mail', 'event': 'Event'}}})
        self.assertEqual(len(mail.outbox), 0)

    def test_invalid_file_users(self):
        file_data = self.read_and_encode('campaigns/tests/files/this_not_excel.docx')

        add_candidates_in_file(str(self.campaign.pk), file_data)
        notification = self.user.notifications.all().first()

        self.assertEqual(notification.verb, 'Upload was broken')
        self.assertEqual(notification.level, Notification.LEVELS.error)
        self.assertEqual(notification.description, 'Error reading file')

    def test_invalid_users_in_file(self):
        file_data = self.read_and_encode('campaigns/tests/files/users_invalid.xls')

        add_candidates_in_file(str(self.campaign.pk), file_data)
        notification = self.user.notifications.all().first()

        self.assertEqual(notification.verb, 'Upload was broken')
        self.assertEqual(notification.level, Notification.LEVELS.error)
        self.assertEqual(notification.description, '1 errors in file')
        self.assertDictEqual(notification.data, {
            'info': True,
            'tip': 'Fix errors in the file and try again',
            'type': 'table',
            'table_data': {
                'table_type': 'candidate_errors',
                'data': [
                    {'lines': '3', 'email': 'somesome.com', 'event': 'Enter a valid e-mail address.'}
                ],
                'header': {'lines': 'Lines', 'email': 'E-mail', 'event': 'Event'}}
        })
        self.assertEqual(len(mail.outbox), 0)

    def test_no_file_data(self):
        file_data = {}

        add_candidates_in_file(str(self.campaign.pk), file_data)
        notification = self.user.notifications.all().first()

        self.assertEqual(notification.verb, 'Upload was broken')
        self.assertEqual(notification.level, Notification.LEVELS.error)
        self.assertEqual(notification.description, 'Error uploading file')
        self.assertEqual(len(mail.outbox), 0)

    def test_empty_file(self):
        file_data = self.read_and_encode('campaigns/tests/files/empty_users.xls')

        add_candidates_in_file(str(self.campaign.pk), file_data)

        notification = self.user.notifications.all().first()

        self.assertEqual(notification.verb, 'Upload was broken')
        self.assertEqual(notification.level, Notification.LEVELS.error)
        self.assertEqual(notification.description, 'File is empty')
        self.assertEqual(len(mail.outbox), 0)

    def test_full_empty_file(self):
        file_data = self.read_and_encode('campaigns/tests/files/full_empty_users.xls')

        add_candidates_in_file(str(self.campaign.pk), file_data)

        notification = self.user.notifications.all().first()

        self.assertEqual(notification.verb, 'Upload was broken')
        self.assertEqual(notification.level, Notification.LEVELS.error)
        self.assertEqual(notification.description, 'File is empty')
        self.assertEqual(len(mail.outbox), 0)

    def test_long_name_error(self):
        file_data = self.read_and_encode('campaigns/tests/files/user_long_name.xls')

        add_candidates_in_file(str(self.campaign.pk), file_data)

        notification = self.user.notifications.all().first()

        self.assertEqual(notification.verb, 'Upload was broken')
        self.assertEqual(notification.level, Notification.LEVELS.error)
        self.assertEqual(notification.data['table_data']['data'], [{'lines': '1',
                                                                    'email': 'some1@email.com',
                                                                    'event': 'Make sure that the candidate name has no '
                                                                             'more than 255 characters'}])
        self.assertEqual(len(mail.outbox), 0)
