# -*- coding: utf-8 -*-
from autofixture import AutoFixture
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.management import BaseCommand
from django.core.validators import EmailValidator

from campaigns.models import Campaign
from users.models import User


class Command(BaseCommand):
    help = "Create campaigns"

    def add_arguments(self, parser):
        parser.add_argument('--email', type=str, dest='email')
        parser.add_argument('--count', type=int, dest='count')

    def handle(self, *args, **options):
        email = options['email']
        count = options['count']
        email_validator = EmailValidator()
        if email and count:
            try:
                email_validator(email)
                user = User.objects.filter(email=email).first()
                if not user:
                    password = User.objects.make_random_password(6)
                    user = User.objects.create_user(email, password)
                    print('-------------------------------')
                    print('password - {}'.format(password))
                    print('-------------------------------')
                AutoFixture(Campaign, field_values={'user': user, 'interview_tree': settings.DEFAULT_INTERVIEW_TREE},
                            generate_fk=True).create(count)
                print('Campaigns created!')
            except ValidationError:
                print('email not valid')
        else:
            print('not count or email')
