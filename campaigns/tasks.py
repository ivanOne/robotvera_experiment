# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

import json
import os
import shutil
import subprocess
import tempfile
import base64
import logging
import uuid
from StringIO import StringIO
import pickle

import requests
import xlrd
import xlwt
from django.core.files.base import ContentFile
from django.core.files.storage import DefaultStorage
from pydub import AudioSegment
from celery import shared_task
from django.conf import settings
from django.utils.translation import ugettext as _
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils import translation
from django.core.files import File
from notifications.models import Notification
from notifications.signals import notify

from campaigns.models import Candidate, Campaign, InterviewRecordItem, CandidateEmotion
from api.serializers import CandidateBulkUploadSerializer
from campaigns.tools import send_interview_entry_mails
from users.models import User

logger = logging.getLogger(__name__)


@shared_task(queue='analyze_emotion')
def analyze_emotion(cand_id):
    candidate = Candidate.objects.filter(pk=cand_id).first()
    overall_emotions_ratio, emotions_percentage_ratio = CandidateEmotion.objects.get_stat_emotions(candidate)
    candidate.interview_emotion_angry = emotions_percentage_ratio['angry']
    candidate.interview_emotion_sad = emotions_percentage_ratio['sad']
    candidate.interview_emotion_happy = emotions_percentage_ratio['happy']
    candidate.interview_emotion_surprised = emotions_percentage_ratio['surprised']
    candidate.interview_emotion_neutral = emotions_percentage_ratio['neutral']
    candidate.interview_emotion_positive = overall_emotions_ratio['positive']
    candidate.interview_emotion_negative = overall_emotions_ratio['negative']

    # Оптимизируем эмоции и сохраняем бэкап
    emotions = candidate.emotions.all().order_by('duration').values_list('data', flat=True)
    data = []
    emotions_for_db = []
    # Строим плоскую структуру эмоций
    for emotion_list in emotions:
        if isinstance(emotion_list, dict):
            data.append(emotion_list['data'])
        else:
            for item in emotion_list:
                data.append(item['data'])
    emotions_length = len(data)
    # px – 1 элемент списка эмоций на 10 пикселей.Точность графика.
    rate = int(round(emotions_length / (settings.LENGTH_VIDEO_PX / settings.EMOTIONS_PX_PRECISION)))
    rate = 1 if rate == 0 else rate
    # Разбиваем эмоции на списки основываясь на rate
    composite_emotions_list = [data[x:x + rate] for x in range(0, emotions_length, rate)]
    # print composite_emotions_list
    for emotion_set in composite_emotions_list:
        angry_sum = 0
        sad_sum = 0
        happy_sum = 0
        surprised_sum = 0
        for item in emotion_set:
            angry_sum += item['angry']
            sad_sum += item['sad']
            happy_sum += item['happy']
            surprised_sum += item['surprised']
        emotions_for_db.append({'angry': angry_sum / rate, 'happy': happy_sum / rate, 'surprised': surprised_sum / rate,
                                'sad': sad_sum / rate})
    candidate.emotions_list = emotions_for_db
    candidate.emotion_backup.save('emotions_{}.json'.format(cand_id), ContentFile(json.dumps(data)), save=False)
    candidate.save(update_fields=['emotion_backup', 'emotions_list', 'interview_emotion_angry', 'interview_emotion_sad',
                                  'interview_emotion_happy', 'interview_emotion_surprised', 'interview_emotion_neutral',
                                  'interview_emotion_positive', 'interview_emotion_negative'])
    candidate.emotions.all().delete()


@shared_task(queue='add_candidates')
def add_candidates_in_file(campaign_id, file_data=None):
    campaign = Campaign.objects.filter(pk=campaign_id).first()
    data_notification = {}
    tip = ''
    notify_data_headers = {'email': _('E-mail'), 'lines': _('Lines'), 'event': _('Event')}

    if campaign:
        translation.activate(campaign.user.language)
        if file_data and file_data.get('file_name') and file_data.get('content') and file_data.get('content_type'):
            content = base64.b64decode(file_data['content'])
            file_upload = SimpleUploadedFile(file_data['file_name'], content, file_data['content_type'])
            try:
                wb = xlrd.open_workbook(file_contents=file_upload.read())
                candidates = list()
                unique_emails = list()
                not_unique_emails = list()
                for ws in wb.sheets():

                    # Если в файле нет кандидатов, делаем нотификацию и выходим из скрипта
                    if ws.nrows <= 1:
                        title_notification = _('Upload was broken')
                        level_notification = Notification.LEVELS.error
                        short_description = _('File is empty')
                        notify.send(campaign.user, recipient=campaign.user, verb=title_notification,
                                    description=short_description, level=level_notification)
                        return

                    for row_num in xrange(1, ws.nrows):
                        row_values = ws.row_values(row_num)
                        row = dict(zip(['name', 'email'], row_values))
                        row['campaign'] = str(campaign.pk)
                        if row.get('email') and row['email']:
                            low_email = row['email'].lower()
                            if low_email not in unique_emails:
                                candidates.append(row)
                                unique_emails.append(row['email'])
                                continue
                            if low_email in unique_emails:
                                not_unique_emails.append({'email': row['email'], 'line': str(row_num+1)})
                                continue
                        else:
                            row['email'] = ''
                            candidates.append(row)

                candidates_serializer = CandidateBulkUploadSerializer(data=candidates, many=True)
                if candidates_serializer.is_valid():
                    result = candidates_serializer.save(owner=campaign.user)
                    title_notification = _('Upload success')
                    level_notification = Notification.LEVELS.success

                    if not_unique_emails:
                        doubles = {}
                        short_description = _('{double_count} duplicates to upload file'
                                              .format(double_count=len(not_unique_emails)))
                        for double in not_unique_emails:
                            if doubles.get(double['email']):
                                doubles[double['email']]['lines'] += '; {}'.format(double['line'])
                                doubles[double['email']]['events'] += 1
                            else:
                                doubles.update({double['email']: {'lines': double['line'], 'events': 1}})
                        flat_doubles = [{'email': e, 'lines': i['lines'],
                                         'event': str(i['events']) + ' matches'} for e, i in doubles.items()]
                        data_notification.update({
                            'type': 'table',
                            'info': True,
                            'table_data': {
                                'table_type': 'candidate_errors',
                                'header': notify_data_headers,
                                'data': flat_doubles
                            }})
                    else:
                        short_description = _('{candidate_count} candidates fully uploaded'
                                              .format(candidate_count=len(candidates)))
                    send_interview_entry_mails(result, campaign.email_text, campaign.user)
                else:
                    errors_count = 0
                    errors = {}
                    level_notification = Notification.LEVELS.error
                    for idx, error in enumerate(candidates_serializer.errors):
                        if error:
                            errors_count += 1
                            email = candidates[idx]['email']
                            lines = str(idx+1)
                            errors_text = ''
                            for field, err_list in error.items():
                                for err_text in err_list:
                                    errors_text += '{}; '.format(err_text)
                            errors_text = errors_text[0:-2]
                            errors.update({email: {'events': errors_text, 'lines': lines}})
                    flat_errors = [{'email': e, 'lines': i['lines'], 'event': i['events']} for e, i in errors.items()]
                    title_notification = _('Upload was broken')
                    short_description = _('{} errors in file'.format(errors_count))
                    tip = _('Fix errors in the file and try again')
                    data_notification.update(
                        {'type': 'table',
                         'info': True,
                         'table_data':
                             {'table_type': 'candidate_errors',
                              'header': notify_data_headers,
                              'data': flat_errors}
                         })
            except xlrd.XLRDError:
                title_notification = _('Upload was broken')
                level_notification = Notification.LEVELS.error
                short_description = _('Error reading file')
        else:
            title_notification = _('Upload was broken')
            level_notification = Notification.LEVELS.error
            short_description = _('Error uploading file')
        if title_notification:
            data_notification['tip'] = tip
            notify.send(campaign.user, recipient=campaign.user, verb=title_notification,
                        description=short_description, level=level_notification, **data_notification)
        translation.activate(settings.LANGUAGE_CODE)

"""
@shared_task(queue='blend_chunk')
def concat_all_audio_interview_chunks(cand_id):
    candidate = Candidate.objects.get(pk=cand_id)

    chunks = candidate.record_items.all().order_by('duration')
    audio_chunks = []
    if chunks.exists():
        for chunk in chunks:
            if not bool(chunk.audio):
                logger.error("No media chunk id={}, audio - {}".format(chunk.id, chunk.audio), extra={'stack': True})
            if bool(chunk.audio):
                audio_chunks.append(chunk.audio.path)
        out_audio, out_audio_filename = tempfile.mkstemp(suffix='.webm')

        with open(out_audio_filename, 'w+b') as result_media:
            for filename in audio_chunks:
                shutil.copyfileobj(open(filename, 'rb'), result_media)
            candidate.audio_interview.save('audio_interview_{}.webm'.format(cand_id), File(result_media), save=False)
            candidate.save(update_fields=['audio_interview'])
        os.remove(out_audio_filename)
        chunks.delete()
"""
@shared_task(queue='blend_chunk')
def concat_all_audio_interview_chunks(cand_id):
    candidate = Candidate.objects.get(pk=cand_id)
    emotion = 'neutral'
    speaker = candidate.campaign.speaker
    speed = 1
    chunks = candidate.record_items.all().order_by('duration')
    playlist = []
    temp_files = []
    if chunks.exists():
        for chunk in chunks:
            if not bool(chunk.audio):
                logger.error("No media chunk id={}, audio - {}".format(chunk.id, chunk.audio), extra={'stack': True})
            if bool(chunk.audio):
                print chunk.audio.path
                if chunk.text:
                    res = requests.get("http://text2audio-m.robotvera.com", params={'text': chunk.text,
                                                                                  'emotion': emotion,
                                                                                  'speaker': speaker,
                                                                                  'speed': speed})
                    out_audio, out_audio_filename = tempfile.mkstemp(suffix='.webm')
                    temp_files.append(out_audio_filename)
                    AudioSegment.from_file(StringIO(res.content), "mp3").export(out_audio_filename, format='webm',
                                                                                codec='libopus')
                    playlist.append(AudioSegment.from_file(out_audio_filename, format='webm', codec='libopus'))
                    playlist.append(AudioSegment.from_file(chunk.audio.path, format='webm', codec='libopus'))
                else:
                    playlist.append(AudioSegment.from_file(chunk.audio.path, format='webm', codec='libopus'))
        out_audio, out_audio_filename = tempfile.mkstemp(suffix='.webm')
        combined = AudioSegment.empty()
        for audio in playlist:
            combined += audio
        result = combined.export(out_audio_filename, format="webm", codec='libopus')
        candidate.audio_interview.save('audio_interview_{}.webm'.format(cand_id), File(result), save=False)
        candidate.save(update_fields=['audio_interview'])
        os.remove(out_audio_filename)
        for temp_file in temp_files:
            os.remove(temp_file)
        chunks.delete()


@shared_task(queue='concat_interview_chunks')
def concat_all_interview_chunks(cand_id):
    candidate = Candidate.objects.get(pk=cand_id)

    chunks = candidate.record_items.all().order_by('duration')
    audio_chunks = []
    video_chunks = []
    if chunks.exists():
        for chunk in chunks:
            if not bool(chunk.video) and not bool(chunk.audio):
                logger.error("No media chunk id={}, audio - {}, video - {}"
                             .format(chunk.id, chunk.audio, chunk.video), extra={'stack': True})
            if bool(chunk.audio):
                audio_chunks.append(chunk.audio.path)
            if bool(chunk.video):
                video_chunks.append(chunk.video.path)
        out_audio, out_audio_filename = tempfile.mkstemp(suffix='.webm')
        out_video, out_video_filename = tempfile.mkstemp(suffix='.webm')

        with open(out_audio_filename, 'wb') as result_media:
            for filename in audio_chunks:
                shutil.copyfileobj(open(filename, 'rb'), result_media)

        with open(out_video_filename, 'wb') as result_media:
            for filename in video_chunks:
                shutil.copyfileobj(open(filename, 'rb'), result_media)

        out_result, out_result_filename = tempfile.mkstemp(suffix='.webm')
        join_data = {'audio': out_audio_filename, 'video': out_video_filename, 'out': out_result_filename}
        cmd = "ffmpeg -i {audio} -i {video} -c copy {out} -y".format(**join_data)
        try:
            subprocess.call(cmd, shell=True)
            with open(out_result_filename) as out_result:
                candidate.interview.save('interview_{}.webm'.format(cand_id), File(out_result), save=False)
                candidate.save(update_fields=['interview'])
        except subprocess.CalledProcessError as err:
            logger.error("Error merge audio and video id={}, ffmpeg command {}".format(cand_id, cmd),
                         extra={'stack': True})
        os.remove(out_audio_filename)
        os.remove(out_video_filename)
        os.remove(out_result_filename)
        chunks.delete()


@shared_task(queue='export_candidates', time_limit=3600)
def export_candidates(user_id, query):
    user = User.objects.get(id=user_id)
    db_query = pickle.loads(query)
    candidates_qs = Candidate.objects.get_queryset()
    candidates_qs.query = db_query

    workbook = xlwt.Workbook()
    sheet = workbook.add_sheet('Candidates transcription')
    for i in range(0, 50):
        sheet.col(i).width = 256 * 80
    sheet.write(0, 0, _('Candidate name'))
    sheet.write(0, 1, _('Invitation date'))
    sheet.write(0, 2, _('Status'))
    sheet.write(0, 3, _('Interview duration'))
    sheet.write(0, 4, _('Note'))
    sheet.write(0, 5, _('Interview'))
    row = 1
    for candidate in candidates_qs:
        sheet.write(row, 0, candidate.name)
        sheet.write(row, 1, candidate.invitation_date.strftime('%d-%m-%Y %H:%M:%S'))
        sheet.write(row, 2, candidate.get_status_display())
        sheet.write(row, 3, candidate.interview_duration if candidate.interview_duration else 0)
        sheet.write(row, 4, candidate.note)
        sheet.write(row, 5, candidate.interview.url if bool(candidate.interview) else '')
        row += 1
    path_parts = [settings.MEDIA_XLS_EXPORTS, '{}-candidates.xls'.format(uuid.uuid4())]
    path = os.path.join(*path_parts)

    f = StringIO()
    workbook.save(f)
    f.seek(0)

    default_storage = DefaultStorage()
    default_storage.save(path, f)
    file_url = default_storage.url(path)
    notify.send(
        user,
        recipient=user,
        verb=_("Data export to Excel is complete."),
        description=_("You can download the file with the export results "
                      "by the <a href='%(link)s' trarget='_blank'>link</a>") % {'link': file_url},
        level=Notification.LEVELS.success, **{'type': 'link', 'url': file_url}
    )
