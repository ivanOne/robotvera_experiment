# -*- coding: utf-8 -*-
from __future__ import unicode_literals, division

from decimal import Decimal
from operator import itemgetter

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Avg
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.utils.translation import ugettext as _

from hashid_field import HashidAutoField
from django.contrib.postgres.fields import JSONField

from users.models import User
from vi_interview.fields import EmailLowerField


class CampaignQS(models.QuerySet):

    def archived(self, *args, **kwargs):
        kwargs.update({'is_archived': True})
        query = self.filter(*args, **kwargs)
        return query

    def unarchived(self, *args, **kwargs):
        kwargs.update({'is_archived': False})
        query = self.filter(*args, **kwargs)
        return query


class Campaign(models.Model):
    class SpeakerYandexRu(object):
        JANE = 'jane'
        OKSANA = 'oksana'
        ALYSS = 'alyss'
        OMAZH = 'omazh'

    SPEAKER_YA_RU_CHOICES = (
        (SpeakerYandexRu.JANE, 'Жанна (Yandex, Russian)'),
        (SpeakerYandexRu.OKSANA, 'Вера (Yandex, Russian)'),
        (SpeakerYandexRu.ALYSS, 'Алиса (Yandex, Russian)'),
        (SpeakerYandexRu.OMAZH, 'Мария (Yandex, Russian)'),
    )

    class SpeakerAmazonRu(object):
        TATYANA = 'Tatyana'

    SPEAKER_AW_RU_CHOICES = (
        (SpeakerAmazonRu.TATYANA, 'Татьяна (Amazon, Russian)'),
    )

    class SpeakerAmazonDk(object):
        NAJA = 'Naja'

    SPEAKER_AW_DK_CHOICES = (
        (SpeakerAmazonDk.NAJA, 'Naja (Amazon, Danish)'),
    )

    class SpeakerAmazonNl(object):
        LOTTE = 'Lotte'

    SPEAKER_AW_NL_CHOICES = (
        (SpeakerAmazonNl.LOTTE, 'Lotte (Amazon, Dutch)'),
    )

    class SpeakerAmazonEn(object):
        # English (Australian)
        NICOLE = 'Nicole'

        # English (British)
        AMY = 'Amy'
        EMMA = 'Emma'

        # English (Indian)
        ADITI = 'Aditi'
        RAVEENA = 'Raveena'

        # English (US)
        IVY = 'Ivy'
        JOANNA = 'Joanna'
        KENDRA = 'Kendra'
        KIMBERLY = 'Kimberly'
        SALLI = 'Salli'

    SPEAKER_AW_EN_CHOICES = (
        (SpeakerAmazonEn.NICOLE, 'Nicole (Amazon, English (Australian))'),

        (SpeakerAmazonEn.AMY, 'Amy (Amazon, English (British))'),
        (SpeakerAmazonEn.EMMA, 'Emma (Amazon, English (British))'),

        (SpeakerAmazonEn.ADITI, 'Aditi (Amazon, English (Indian))'),
        (SpeakerAmazonEn.RAVEENA, 'Raveena (Amazon, English (Indian))'),

        (SpeakerAmazonEn.IVY, 'Ivy (Amazon, English (US))'),
        (SpeakerAmazonEn.JOANNA, 'Joanna (Amazon, English (US))'),
        (SpeakerAmazonEn.KENDRA, 'Kendra (Amazon, English (US))'),
        (SpeakerAmazonEn.KIMBERLY, 'Kimberly (Amazon, English (US))'),
        (SpeakerAmazonEn.SALLI, 'Salli (Amazon, English (US))')
    )

    class SpeakerAmazonDe(object):
        MARLENE = 'Marlene'
        VICKI = 'Vicki'

    SPEAKER_AW_DE_CHOICES = (
        (SpeakerAmazonDe.MARLENE, 'Marlene (Amazon, German)'),
        (SpeakerAmazonDe.VICKI, 'Vicki (Amazon, German)'),
    )

    class SpeakerAmazonFr(object):
        # French
        CELINE = 'Celine'

    SPEAKER_AW_FR_CHOICES = (
        (SpeakerAmazonFr.CELINE, 'Céline (Amazon, French)'),
    )

    class SpeakerAmazonIs(object):
        DORA = 'Dora'

    SPEAKER_AW_IS_CHOICES = (
        (SpeakerAmazonIs.DORA, 'Dóra (Amazon, Icelandic)'),
    )

    class SpeakerAmazonIt(object):
        CARLA = 'Carla'

    SPEAKER_AW_IT_CHOICES = (
        (SpeakerAmazonIt.CARLA, 'Carla (Amazon, Italian)'),
    )

    class SpeakerAmazonNo(object):
        LIV = 'Liv'

    SPEAKER_AW_NO_CHOICES = (
        (SpeakerAmazonNo.LIV, 'Liv (Amazon, Norwegian)'),
    )

    class SpeakerAmazonPl(object):
        EWA = 'Ewa'
        MAJA = 'Maja'

    SPEAKER_AW_PL_CHOICES = (
        (SpeakerAmazonPl.EWA, 'Ewa (Amazon, Polish)'),
        (SpeakerAmazonPl.MAJA, 'Maja (Amazon, Polish)'),
    )

    class SpeakerAmazonPr(object):
        # Portuguese (Brazilian)
        VITORIA = 'Vitoria'

        # Portuguese (European)
        INES = 'Ines'

    SPEAKER_AW_PR_CHOICES = (
        (SpeakerAmazonPr.VITORIA, 'Vitória (Amazon, Portuguese (Brazilian))'),
        (SpeakerAmazonPr.INES, 'Inês (Amazon, Portuguese (European))'),
    )

    class SpeakerAmazonRo(object):
        CARMEN = 'Carmen'

    SPEAKER_AW_RO_CHOICES = (
        (SpeakerAmazonRo.CARMEN, 'Carmen (Amazon, Romanian)'),
    )

    class SpeakerAmazonEs(object):
        # Spanish (Castilian)
        CONCHITA = 'Conchita'

        # Spanish (Latin American)
        PENELOPE = 'Penelope'

    SPEAKER_AW_ES_CHOICES = (
        (SpeakerAmazonEs.CONCHITA, 'Conchita (Amazon, Spanish (Castilian))'),
        (SpeakerAmazonEs.PENELOPE, 'Penélope (Amazon, Spanish (Latin American))'),
    )

    class SpeakerAmazonSe(object):
        ASTRID = 'Astrid'

    SPEAKER_AW_SE_CHOICES = (
        (SpeakerAmazonSe.ASTRID, 'Astrid (Amazon, Swedish)'),
    )

    class SpeakerAmazonTr(object):
        FILIZ = 'Filiz'

    SPEAKER_AW_TR_CHOICES = (
        (SpeakerAmazonTr.FILIZ, 'Filiz (Amazon, Turkish)'),
    )

    SPEAKER_GOOGLE_CHOICES = (
        ('ja-JP-Wavenet-A', 'ja-JP-Wavenet-A (Google)'),
    )

    class SpeakerAmazonJp(object):
        MIZUKI = 'Mizuki'
        TAKUMI = 'Takumi'

    SPEAKER_AW_JP_CHOICES = (
        (SpeakerAmazonJp.MIZUKI, 'Mizuki (Amazon, Japanese)'),
        (SpeakerAmazonJp.TAKUMI, 'Takumi (Amazon, Japanese)'),
    )

    ALL_SPEAKERS_CHOICES = (
            SPEAKER_YA_RU_CHOICES + SPEAKER_AW_RU_CHOICES + SPEAKER_AW_DK_CHOICES +
            SPEAKER_AW_NL_CHOICES + SPEAKER_AW_EN_CHOICES + SPEAKER_AW_DE_CHOICES +
            SPEAKER_AW_FR_CHOICES + SPEAKER_AW_IS_CHOICES + SPEAKER_AW_IT_CHOICES +
            SPEAKER_AW_NO_CHOICES + SPEAKER_AW_PL_CHOICES + SPEAKER_AW_PR_CHOICES +
            SPEAKER_AW_RO_CHOICES + SPEAKER_AW_ES_CHOICES + SPEAKER_AW_SE_CHOICES +
            SPEAKER_AW_TR_CHOICES + SPEAKER_GOOGLE_CHOICES + SPEAKER_AW_JP_CHOICES
    )

    class Category(object):
        HIRING = 0
        ASSESSMENT = 1
        ADAPTATION = 2
        EMPLOYEE_SURVEY = 3
        EXIT_INTERVIEW = 4

    CATEGORY_CHOICES = (
        (Category.HIRING, _('Hiring')),
        (Category.ASSESSMENT, _('Assessment')),
        (Category.ADAPTATION, _('Adaptation')),
        (Category.EMPLOYEE_SURVEY, _('Employee survey')),
        (Category.EXIT_INTERVIEW, _('Exit interview')),
    )

    class InterviewType(object):
        VIDEO_INTERVIEW = 'video'
        AUDIO_INTERVIEW = 'audio'

    INTERVIEW_TYPE_CHOICES = (
        (InterviewType.VIDEO_INTERVIEW, _('Video interview')),
        (InterviewType.AUDIO_INTERVIEW, _('Audio interview')),
    )

    id = HashidAutoField(primary_key=True)
    name = models.CharField(verbose_name='Name', max_length=255)
    description = models.TextField(verbose_name='Description', null=True, blank=True)
    interview_tree = JSONField(null=True, blank=True)
    speaker = models.CharField(max_length=16, choices=ALL_SPEAKERS_CHOICES, default=SpeakerAmazonEn.AMY, blank=True)
    email_text = models.TextField(verbose_name='E-mail text', null=True, blank=True)
    user = models.ForeignKey(User, verbose_name='User')
    category = models.PositiveIntegerField(verbose_name='Category', default=0, choices=CATEGORY_CHOICES)
    interview_type = models.CharField(verbose_name='Interview type', choices=INTERVIEW_TYPE_CHOICES,
                                      default=InterviewType.VIDEO_INTERVIEW, max_length=10)
    is_archived = models.BooleanField(verbose_name='Archived', default=False)
    created_at = models.DateTimeField(verbose_name='Date created', auto_now_add=True)
    modified_at = models.DateTimeField(verbose_name='Date updated', auto_now=True)

    def delete_to_archive(self):
        self.is_archived = True
        self.save()
        self.campaign_candidates.all().update(is_archived=True)

    objects = CampaignQS.as_manager()

    def __unicode__(self):
        return '%s (%s)' % (self.id, self.name)

    class Meta:
        ordering = ['-created_at']


class CandidateQS(models.QuerySet):

    def archived(self, *args, **kwargs):
        kwargs.update({'is_archived': True})
        query = self.filter(*args, **kwargs)
        return query

    def unarchived(self, *args, **kwargs):
        kwargs.update({'is_archived': False})
        query = self.filter(*args, **kwargs)
        return query


class Candidate(models.Model):
    class Status(object):
        INVITED = 0
        START_INTERVIEW = 1
        FINISH_INTERVIEW = 2

    class InterviewStatus(object):
        STOP = 0
        END = 1
        NOT_INTERVIEW = 2

    STATUS_CHOICES = (
        (Status.INVITED, _('Invited')),
        (Status.START_INTERVIEW, _('Started the interview')),
        (Status.FINISH_INTERVIEW, _('Finished the interview')),
    )

    INTERVIEW_STATUS_CHOICES = (
        (InterviewStatus.NOT_INTERVIEW, _('No interview')),
        (InterviewStatus.STOP, _('Interview stopped')),
        (InterviewStatus.END, _('Interview end')),
    )

    id = HashidAutoField(primary_key=True)
    name = models.CharField(verbose_name='Name', max_length=255, null=True, blank=True)
    email = EmailLowerField(verbose_name='Candidate email')
    status = models.PositiveIntegerField(verbose_name='Status', choices=STATUS_CHOICES, default=Status.INVITED)
    note = models.TextField(verbose_name='Note', null=True, blank=True)
    invitation_date = models.DateTimeField(verbose_name='Invitation Date', auto_now_add=True)
    interview = models.FileField(verbose_name='Interview record', null=True, blank=True,
                                 upload_to=settings.MEDIA_INTERVIEW)
    audio_interview = models.FileField(verbose_name='Audio interview record', null=True, blank=True,
                                       upload_to=settings.MEDIA_AUDIO_INTERVIEW)
    interview_status = models.IntegerField(verbose_name='Interview status', choices=INTERVIEW_STATUS_CHOICES,
                                           default=InterviewStatus.NOT_INTERVIEW)
    interview_duration = models.DecimalField(verbose_name='Video duration', decimal_places=1, max_digits=12,
                                             validators=[MinValueValidator(Decimal('0.0'))], default=Decimal('0.0'))
    campaign = models.ForeignKey(Campaign, verbose_name='Campaign', related_name='campaign_candidates')
    owner = models.ForeignKey(User, verbose_name='Owner', related_name='owner_candidates')
    is_archived = models.BooleanField(verbose_name='Archived', default=False)
    human_score_avg = models.DecimalField('Human score AVG', max_digits=10, decimal_places=4, null=True, blank=True)

    emotion_backup = models.FileField('Emotion backup', null=True, blank=True, upload_to='candidate_emotions')
    emotions_list = JSONField(null=True, blank=True)

    interview_emotion_positive = models.PositiveSmallIntegerField('Positive', default=0)
    interview_emotion_negative = models.PositiveSmallIntegerField('Negative', default=0)
    interview_emotion_neutral = models.PositiveSmallIntegerField('Neutral', default=0)
    interview_emotion_happy = models.PositiveSmallIntegerField('Happy', default=0)
    interview_emotion_surprised = models.PositiveSmallIntegerField('Surprised', default=0)
    interview_emotion_angry = models.PositiveSmallIntegerField('Angry', default=0)
    interview_emotion_sad = models.PositiveSmallIntegerField('Sad', default=0)

    def delete_to_archive(self):
        self.is_archived = True
        self.save()

    def update_avg_human_score(self):
        self.human_score_avg = self.transcriptions.filter(human_score__isnull=False) \
            .aggregate(human_avg_score=Avg('human_score'))['human_avg_score']
        self.save()

    def __str__(self):
        return '%s (%s)' % (self.id, self.email)

    objects = CandidateQS.as_manager()

    class Meta:
        unique_together = (('campaign', 'email'),)
        ordering = ['-invitation_date']


class InterviewRecordItem(models.Model):
    candidate = models.ForeignKey(Candidate, related_name='record_items')
    video = models.FileField(null=True, blank=True, upload_to='video_items', storage=FileSystemStorage())
    audio = models.FileField(null=True, blank=True, upload_to='audio_items', storage=FileSystemStorage())
    text = models.TextField(null=True, blank=True)
    duration = models.PositiveIntegerField(_('Time'))

    class Meta:
        ordering = ['duration']


class CandidateTranscription(models.Model):
    candidate = models.ForeignKey(Candidate, related_name='transcriptions')
    duration = models.PositiveIntegerField(_('Time'))
    question = models.TextField(_('Question'))
    answer = models.TextField(_('Answer'), null=True, blank=True)
    etalon = models.TextField(_('Etalon answer'), null=True, blank=True)
    human_score = models.SmallIntegerField(_('AI Score'), null=True, blank=True)
    info = models.TextField(_('Transcript info'), null=True, blank=True)

    class Meta:
        unique_together = ('duration', 'candidate')
        ordering = ['duration']


class CandidateEmotionManager(models.Manager):
    use_in_migrations = True

    def get_stat_emotions(self, candidate):
        total_duration = candidate.interview_duration
        overall_data_percent = {'happy': 0, 'surprised': 0, 'neutral': 0, 'angry': 0, 'sad': 0}
        emotions_db = self.filter(candidate=candidate)
        if not total_duration or not emotions_db:
            return {'positive': 0, 'negative': 0}, overall_data_percent
        emotions = []

        # Все эмоции в один список
        for emotion in emotions_db:
            if isinstance(emotion.data, list):
                for data_item in emotion.data:
                    emotions.append(data_item)
            elif isinstance(emotion.data, dict):
                emotions.append(emotion.data)

        # Сортируем список в хронологическом порядке
        emotions_list = sorted(emotions, key=itemgetter('time'))

        # Поддержка кандидатов у которых interview_duration в секундах
        if total_duration < emotions_list[-1]['time']:
            total_duration *= 1000
        range_emotions_list = []

        # Функция для работы с текущей эмоцией
        def manage_current_high_emotion(high_emotion, current_emotion_data, raw_emotion, last_emotion):
            # Проверяем есть ли уже текущая эмоция и если она равна выраженной эмоции,
            # то меняем дату окончания на текущую
            if current_emotion_data.get('emotion') and current_emotion_data['emotion'] == high_emotion:
                current_emotion_data['end'] = raw_emotion.get('time')

            # Если выраженная эмоция не соответсвует текущей, то проставляем текущей эмоции дату окончания,
            # и добавлем в список эмоций, заполняя при этом текущую эмоцию новыми данными
            elif current_emotion_data.get('emotion') and current_emotion_data['emotion'] != high_emotion:
                current_emotion_data['end'] = emotion.get('time')
                range_emotions_list.append(current_emotion_data.copy())
                current_emotion_data['emotion'] = high_emotion
                current_emotion_data['start'] = emotion.get('time')
                current_emotion_data['end'] = ''

            # Если текущая эмоция не определена, значит мы находимся в самом начале интервью, и просто заполняем
            # информацией о текущей эмоции
            elif current_emotion_data.get('emotion') is None:
                current_emotion_data['emotion'] = high_emotion
                current_emotion_data['start'] = 0
            if last_emotion:
                current_emotion_data['end'] = total_duration
                range_emotions_list.append(current_emotion_data.copy())

        range_emotion = {}
        items_count = len(emotions_list) - 1
        for idx, emotion in enumerate(emotions_list):
            emotion_data = emotion['data']
            # Вычисляем две самых выделенных эмоции и вычисляем их разность в процентах
            high_keys = sorted(emotion_data, key=emotion_data.get, reverse=True)[:2]
            if not emotion_data[high_keys[0]] or not emotion_data[high_keys[1]]:
                continue
            result = ((emotion_data[high_keys[0]] - emotion_data[high_keys[1]]) / emotion_data[high_keys[0]]) * 100
            last_iteration = idx == items_count

            # Эмоция выраженная
            if result > settings.PERCENT_DIFF_EMOTION:
                manage_current_high_emotion(high_keys[0], range_emotion, emotion, last_iteration)

            # Эмоция не выраженная, считаем что нейтральная
            else:
                manage_current_high_emotion('neutral', range_emotion, emotion, last_iteration)

        overall_data = {'happy': 0, 'surprised': 0, 'neutral': 0, 'angry': 0, 'sad': 0}
        overall_data_percent = {'happy': 0, 'surprised': 0, 'neutral': 0, 'angry': 0, 'sad': 0}

        # Вычисляем тайминг по каждой эмоции
        for emotion_range in range_emotions_list:
            duration = emotion_range['end'] - emotion_range['start']
            overall_data[emotion_range['emotion']] += duration
        # Вычисляем доли и общие эмоции
        for emotion, duration in overall_data.items():
            overall_data_percent[emotion] = int(round((duration / total_duration) * 100))
        positive_negative_data = {'positive': overall_data_percent['happy'] + overall_data_percent['surprised'],
                                  'negative': overall_data_percent['angry'] + overall_data_percent['sad']}
        # Хотим красивые целые цифры, алгоритм простой
        total = overall_data_percent['happy'] + overall_data_percent['surprised'] + overall_data_percent['angry'] + \
                overall_data_percent['sad']
        neutral = 100 - total
        if neutral < 0:
            overall_data_percent['neutral'] = 0
        else:
            overall_data_percent['neutral'] = neutral
        return positive_negative_data, overall_data_percent


class CandidateEmotion(models.Model):
    candidate = models.ForeignKey(Candidate, related_name='emotions')
    data = JSONField(null=True, blank=True)
    duration = models.PositiveIntegerField('Time')

    objects = CandidateEmotionManager()

    class Meta:
        unique_together = ('duration', 'candidate')
        ordering = ['duration']


@receiver(pre_delete, sender=InterviewRecordItem)
def clean_file(**kwargs):
    instance = kwargs.get('instance')
    instance.video.delete(save=False)
    instance.audio.delete(save=False)
