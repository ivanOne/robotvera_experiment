# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.core.cache import cache
from django.utils import translation
from django.utils.encoding import smart_text
from django.utils.translation import ugettext as _
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from campaigns.models import Candidate, Campaign
from dictionary.serializer import TranslateSerializer


class BaseDictionaryView(APIView):
    choices = None
    free_structure = False

    def convert_choices_to_translation_dict(self, choices):
        data = []
        for d in choices:
            text = d[1]
            data.append({'key': d[0], 'name': smart_text(_(text))})
        return data

    # Реализуется либо данный метод либо данные берутся из choices
    def get_data(self):
        raise NotImplementedError('Method get_data not implemented')

    def get_dict_key(self):
        return self.request.path.split('/')[-2]

    def get_cached_dictionary(self, lang):
        cache_key = settings.CACHES_KEYS['trans_dictionary'].format(**{'lang': lang, 'category': self.get_dict_key()})
        data = cache.get(cache_key)
        if not data:
            if self.free_structure:
                data = self.get_data()
            else:
                if not self.choices:
                    translated_dictionary = self.get_data()
                else:
                    translated_dictionary = self.convert_choices_to_translation_dict(self.choices)
                data = TranslateSerializer(translated_dictionary, many=True).data
            cache.set(cache_key, data)
        return data

    def get(self, request, *args, **kwargs):
        cur_language = translation.get_language()
        lang = request.GET.get('lang', cur_language)
        with translation.override(lang):
            dictionary = self.get_cached_dictionary(lang)
        return Response(dictionary, status=status.HTTP_200_OK)


class CandidateStatusDict(BaseDictionaryView):
    choices = Candidate.STATUS_CHOICES


class CampaignCategories(BaseDictionaryView):
    choices = Campaign.CATEGORY_CHOICES


class Speakers(BaseDictionaryView):
    choices = Campaign.ALL_SPEAKERS_CHOICES


class LangSpeakers(BaseDictionaryView):
    free_structure = True

    def get_data(self):
        return {
            'ru': [Campaign.SpeakerYandexRu.JANE, Campaign.SpeakerYandexRu.OKSANA, Campaign.SpeakerYandexRu.ALYSS,
                   Campaign.SpeakerYandexRu.OMAZH, Campaign.SpeakerAmazonRu.TATYANA],

            # Датский
            'da': [Campaign.SpeakerAmazonDk.NAJA],

            # Нидерландский
            'nl': [Campaign.SpeakerAmazonNl.LOTTE],

            # Английский
            'en-US': [Campaign.SpeakerAmazonEn.IVY, Campaign.SpeakerAmazonEn.JOANNA, Campaign.SpeakerAmazonEn.KENDRA,
                      Campaign.SpeakerAmazonEn.KIMBERLY, Campaign.SpeakerAmazonEn.SALLI],
            'en-AU': [Campaign.SpeakerAmazonEn.NICOLE],
            'en-GB': [Campaign.SpeakerAmazonEn.AMY, Campaign.SpeakerAmazonEn.EMMA],
            'en-IN': [Campaign.SpeakerAmazonEn.ADITI, Campaign.SpeakerAmazonEn.RAVEENA],

            # Немецкий
            'de': [Campaign.SpeakerAmazonDe.MARLENE, Campaign.SpeakerAmazonDe.VICKI],

            # Французский
            'fr': [Campaign.SpeakerAmazonFr.CELINE],

            # Исландский
            'is': [Campaign.SpeakerAmazonIs.DORA],

            # Итальянский
            'it': [Campaign.SpeakerAmazonIt.CARLA],

            # Норвежский
            'no': [Campaign.SpeakerAmazonNo.LIV],

            # Польский
            'pl': [Campaign.SpeakerAmazonPl.EWA, Campaign.SpeakerAmazonPl.MAJA],

            # Португальский
            'pt-PT': [Campaign.SpeakerAmazonPr.INES],
            'pt-BR': [Campaign.SpeakerAmazonPr.VITORIA],

            # Румынский
            'ro': [Campaign.SpeakerAmazonRo.CARMEN],

            # Испанский
            'es': [Campaign.SpeakerAmazonEs.CONCHITA, Campaign.SpeakerAmazonEs.PENELOPE],

            # Шведский
            'sv': [Campaign.SpeakerAmazonSe.ASTRID],

            # Турецкий
            'tr': [Campaign.SpeakerAmazonTr.FILIZ],

            # Японский
            'ja-JP': ['ja-JP-Wavenet-A', Campaign.SpeakerAmazonJp.MIZUKI, Campaign.SpeakerAmazonJp.TAKUMI]
        }