# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from unittest import skip

from django.core.cache import cache
from django.core.management import call_command
from django.test import TestCase, override_settings
from rest_framework import status


class DictionaryTestCase(TestCase):

    def setUp(self):
        self.status_data = [
            {
                "key": "0",
                "name": "Invited"
            },
            {
                "key": "1",
                "name": "Started the interview"
            },
            {
                "key": "2",
                "name": "Finished the interview"
            }
        ]

    @override_settings(CACHES={'default': {'BACKEND': 'django.core.cache.backends.dummy.DummyCache'}})
    def test_en_candidate_status_cat_dict(self):
        resp = self.client.get('/api/dictionaries/candidate_status/?lang=en')

        self.assertListEqual(resp.data, self.status_data)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    @override_settings(CACHES={'default': {'BACKEND': 'django.core.cache.backends.dummy.DummyCache'}})
    def test_not_valid_lang_param(self):
        resp = self.client.get('/api/dictionaries/candidate_status/?lang=55')

        self.assertListEqual(resp.data, self.status_data)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_cached_dictionaries(self):
        cache.delete('en_candidate_status_translate_dict')
        self.client.get('/api/dictionaries/candidate_status/?lang=en')
        data = cache.get('en_candidate_status_translate_dict')

        self.assertListEqual(data, self.status_data)

    @skip(u'не юзаем до подключения redis')
    def test_translate_cache_clear_management_command(self):
        call_command('clear_cache_dictionaries')
        self.client.get('/api/dictionaries/candidate_status/?lang=en')
        data = cache.get('en_candidate_status_translate_dict')

        self.assertListEqual(data, self.status_data)

        call_command('clear_cache_dictionaries')
        data = cache.get('en_candidate_status_translate_dict')

        self.assertIsNone(data)
