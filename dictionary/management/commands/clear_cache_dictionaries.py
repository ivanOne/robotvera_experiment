# -*- coding: utf-8 -*-
from django.core.cache import cache
from django.core.management import BaseCommand


class Command(BaseCommand):
    help = "Clear translate dictionaries cache"

    def handle(self, *args, **options):
        cache.delete_pattern('*_translate_dict')
