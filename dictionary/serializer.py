# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import serializers


class TranslateSerializer(serializers.Serializer):
    key = serializers.CharField()
    name = serializers.CharField()
