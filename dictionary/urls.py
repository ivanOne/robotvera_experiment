from django.conf.urls import url
import views

urlpatterns = [
    url(r'^candidate_status/$', views.CandidateStatusDict.as_view()),
    url(r'^campaign_categories/$', views.CampaignCategories.as_view()),
    url(r'^speakers/$', views.Speakers.as_view()),
    url(r'^speakers_lang_mapping/$', views.LangSpeakers.as_view()),
]